module.exports = {
    plugins: {
        'postcss-pxtorem': {
            rootValue: 16, // 已设计稿宽度1920px为例
            propList: ['*'],
        },
    },
};
