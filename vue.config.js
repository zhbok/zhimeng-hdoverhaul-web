const { name } = require('./package');
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
module.exports = {
  publicPath: './',
  // assetsDir: 'hdoverhaul',
  // publicPath: 'http://10.180.238.11:32377/hdoverhaul/',
  productionSourceMap: false,
  // transpileDependencies: [true],
  devServer: {
    port: 9090,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    proxy: {
      // '/api/dangerPoint': {
      //   target: 'http://192.168.100.140:8080/dangerPoint',
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/api/dangerPoint': ''
      //   }
      // },
      // '/api/authority': {
      //   target: 'http://192.168.100.140:8080/authority',
      //   // target: 'http://172.16.218.116:8082',
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/api/authority': ''
      //   }
      // },
      // '/api/bank': {
      //   target: 'http://192.168.100.140:8080/bank',
      //   // target: 'http://172.16.218.116:8082',
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/api/bank': ''
      //   }
      // },
      '/api/workOrder': {
        target: 'http://192.168.1.61:10068',
        // target: 'http://172.16.218.116:8082',
        changeOrigin: true,
        pathRewrite: {
          // '^/push': ''
        }
      },
      '/api': {
        // target: 'http://192.168.1.177:8089',
        // target: 'http://192.168.1.245:8089',
        target: 'http://192.168.1.85:15303/sac/fresh-hdoverhaul-provider/v3',
        // target: 'http://172.27.16.85:15303/fresh/hdoverhaul/v1',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  configureWebpack: {
    // 开启分离 js
    optimization: {
      //   runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        //     maxInitialRequests: Infinity,
        //     minSize: 20000,
        //     cacheGroups: {
        //       vendor: {
        //         test: /[\\/]node_modules[\\/]/,
        //         name(module) {
        //           // get the name. E.g. node_modules/packageName/not/this/part.js
        //           // or node_modules/packageName
        //           const hdpackageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
        //           // npm package names are URL-safe, but some servers don't like @ symbols
        //           return `npm.${hdpackageName.replace('@', '')}`
        //         }
        //       }
        //     }
      }
    },
    // plugins: [
    //   // 分析打包原因
    //   new BundleAnalyzerPlugin(),
    // ],
    output: {
      library: `${name}-[name]`,
      libraryTarget: 'umd', // 把微应用打包成 umd 库格式
      // libraryTarget: 'window',
      jsonpFunction: `webpackJsonp_${name}`
    },
    devtool: process.env.ENV === 'production' ? false : "source-map"
  },
  chainWebpack(config) {
    //   config.plugins.delete('prefetch');
    //   config.module
    //     .rule('fonts')
    //     .use('url-loader')
    //     .loader('url-loader')
    //     .options({
    //       limit: 4096, // 小于4kb将会被打包成 base64
    //       fallback: {
    //         loader: 'file-loader',
    //         options: {
    //           esModule: false,
    //           name: 'hdoverhaul/fonts/[name].[hash:8].[ext]',
    //           publicPath: './'
    //         }
    //       }
    //     })
    //     .end();
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .options({
        limit: 999999,//  base64
        fallback: {
          loader: 'file-loader',
          options: {
            esModule: false,
            name: 'hdoverhaul/img/[name].[hash:8].[ext]',
            publicPath: './'
          }
        }
      })
  },
  lintOnSave: false, //关闭eslint检查
  pages: {
    index: 'src/main.js',
    subpage: 'src/subpage.js'
  }
};
