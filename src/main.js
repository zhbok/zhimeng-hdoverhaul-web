import './public-path';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import routes from './router';
import store from './store';
import './assets/styles/index.scss';
import './assets/styles/element.scss';
import './assets/css/theme/index.css';
import globalfunc from './utils/globalfunc';
import { initSubAppRouterGuards } from './utils/permission';
import 'normalize.css';
import QiankunCt from './Qiankun';
// import * as echarts from 'echarts';
import lodash, { get } from 'lodash';
import { routesQiankunHashModeHandler } from '@/utils/routesQiankunHashModeHandler';
import sacJson from '../public/sac.json';
// import { setToken } from '@/utils/auth';
import 'element-ui/lib/theme-chalk/display.css';
import { getCookie } from '@/utils/storage';
import { globalState } from '@/plugins/globalState';
import ElementUI from 'element-ui';
import { TableColumn } from 'element-ui';
import selectArea from '@/components/selectArea'; // 导入片区组件
import selectStation from '@/components/selectStation'; // 导入场站组件

Vue.component(selectArea.name, selectArea);
Vue.component(selectStation.name, selectStation);

Vue.prototype.$lodash = lodash;
// Vue.prototype.$echarts = echarts;
Vue.config.productionTip = false;

//注册全局函数
Vue.prototype.globalfunc = globalfunc;

TableColumn.props.showOverflowTooltip = {
  type: Boolean,
  default: true
};
// import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI, {
  size: getCookie('size') || 'small',
  zIndex: 3000,
  scoped: false
});

ElementUI.Input.props.clearable = { type: Boolean, default: true };
ElementUI.Select.props.clearable = { type: Boolean, default: true };
ElementUI.Dialog.props['close-on-click-modal'] = {
  type: Boolean,
  default: false
};
ElementUI.Dialog.props['destroy-on-close'] = { type: Boolean, default: true };

let instance = null;
let router = null;
const nzApplicationName = sacJson.qiankun.name;

// 将px转为rem，方便与内联样式转换
function px2rem(px) {
  if (/%/gi.test(px)) {
    // 有百分号%，特殊处理，表述pc是一个有百分号的数，比如：90%
    return px;
  } else {
    if (/px/gi.test(px)) {
      // 有px，特殊处理，表述是一个有带有px的数，比如：90px
      let num = px.split('px')[0];
      return parseFloat(num) / 16 + 'rem';
    } else {
      return parseFloat(px) / 16 + 'rem';
    }
  }
}
Vue.prototype.$px2rem = px2rem;
async function render(props = {}) {
  const { container } = props;
  // console.log(props, 'props');

  if (container) {
    // await store.dispatch('getUserInfo', '1171471009513996288'); // 1171471009513996288  测试环境平台系统id
    // await store.dispatch('getUserInfo', '1186382879806980096'); // 1186382879806980096  生产环境平台系统id
    window.__MICRO_ROUTE_PREFIX__ = props.prefixRoute;
    router = new VueRouter({
      routes: routesQiankunHashModeHandler(routes, true)
    });
    initSubAppRouterGuards(router, props);
    globalState.initGlobalState(props);
  } else {
    router = new VueRouter({
      routes
    });
  }

  instance = new Vue({
    router,
    store,
    render: h => h(container ? QiankunCt : App)
  }).$mount(container ? container.querySelector('#app') : '#app');
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {
  console.log(`[${nzApplicationName}] bootstrap.`);
}

export async function mount(props) {
  Vue.prototype.$parentRouter = props.singleSpa;
  // props.onGlobalStateChange((state, prev) => {
  //   // setToken(state.token);
  //   // console.log(state, prev, 'props123');
  //   // store.dispatch('user/setToken', state.token)
  //   localStorage.setItem('main-app-token', state.token);
  // });
  props.setGlobalState({
    api: 'get:token',
    from: nzApplicationName,
    to: 'main'
  });
  // 南自示例中提供，后续可以看看如何改进
  setTimeout(() => {
    props.setGlobalState({
      api: 'get:token',
      from: nzApplicationName,
      to: 'main'
    });
  }, 2000);
  render(props);
}
export async function unmount() {
  console.log('[vue] vue app unmount');
}
