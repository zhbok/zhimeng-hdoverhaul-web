const PLATEFORM = ['分级诊断', '远程监控', '智能巡检', '技术监督'].map((item) => {
  return { id: item, name: item }
})

const OPERATIONTYPE = {
  'ADD': 0,
  'DEL': 1,
  'EDIT': 2,
  'DETAIL': 3,
  'APPROVE': 4,
}

export {
  PLATEFORM
}