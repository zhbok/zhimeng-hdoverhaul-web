class GlobalState {
  isQK = false;
  state = {};

  initGlobalState(props) {
    this.isQK = true;
    this.state = props;
  }

  setGlobalState(...args) {
    if (!this.isQK) {
      return;
    }
    this.state.setGlobalState(...args);
  }

  onGlobalStateChange(...args) {
    if (!this.isQK) {
      return;
    }
    this.state.onGlobalStateChange(...args);
  }
}
export const globalState = new GlobalState();
