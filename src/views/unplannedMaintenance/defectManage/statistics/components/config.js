import { fontSize } from '@/utils';
// 缺陷月度统计
export const histogram = data => {
  let xData = data.map(v => v.month + '月');
  let yData1 = data.map(v => (v.num == 'null' ? 0 : v.num));
  let yData2 = data.map(v => (v.processed == 'null' ? 0 : v.processed));
  let yData3 = data.map(v => (v.pending == 'null' ? 0 : v.pending));
  // let yData4 = data.map(v => Number(v.rate));
  // let yData5 = data.map(v => Number(v.yoy));
  // let yData6 = data.map(v => Number(v.ratio));
  let yData4 = data.map(v => (v.rate == null ? 0 : Number(v.rate)));
  let yData5 = data.map(v => (v.yoy == null ? 0 : Number(v.yoy)));
  let yData6 = data.map(v => (v.ratio == null ? 0 : Number(v.ratio)));
  let option = {
    backgroundColor: 'transparent',
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    legend: {
      data: ['缺陷数', '已处理', '待处理', '消缺率', '同比率', '环比率'],
      x: 'center',
      textStyle: {
        color: '#7f7f7f'
      },
      itemWidth: 10,
      itemHeight: 10,
      itemGap: 25
    },
    grid: {
      top: '30%',
      left: '3%',
      right: '3%',
      bottom: '0%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: xData,
        axisLine: {
          show: true,
          lineStyle: {
            color: '#E4E7ED',
            width: 1,
            type: 'solid'
          }
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          show: true,
          color: '#7f7f7f'
        }
      }
    ],
    yAxis: [
      {
        name: '单位：个',
        nameTextStyle: {
          color: '#7f7f7f'
          // padding: [0, 10, 0, 0]
        },
        type: 'value',
        min: 0,
        minInterval: 1,
        axisLabel: {
          color: '#7f7f7f',
          fontSize: fontSize(14)
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: false,
          lineStyle: {
            color: '#E4E7ED',
            width: 1,
            type: 'solid'
          }
        },
        splitLine: {
          lineStyle: {
            color: '#E4E7ED'
          }
        }
      },
      {
        name: '单位：%',
        nameTextStyle: {
          color: '#7f7f7f'
          // padding: [0, 0, 0, 35]
        },
        type: 'value',
        splitLine: {
          show: false
        },
        // min: -100,
        min: 0,
        max: 100,
        interval: 20,
        axisLabel: {
          interval: 0,
          color: '#7f7f7f',
          fontSize: 10,
          formatter: '{value}'
        }
      }
    ],
    series: [
      {
        name: '缺陷数',
        type: 'bar',
        data: yData1,
        barWidth: 10, //柱子宽度
        barGap: 0.5, //柱子之间间距
        itemStyle: {
          color: '#016fff'
        }
      },
      {
        name: '已处理',
        type: 'bar',
        data: yData2,
        barWidth: 10,
        barGap: 0.5,
        itemStyle: {
          color: '#00b999'
        }
      },
      {
        name: '待处理',
        type: 'bar',
        data: yData3,
        barWidth: 10,
        barGap: 0.5,
        itemStyle: {
          color: '#ffb061'
        }
      },
      {
        name: '消缺率',
        type: 'line',
        yAxisIndex: 1,
        symbolSize: 0,
        symbol: 'emptyCircle',
        itemStyle: {
          color: '#ff624a',
          borderRadius: '100%',
          borderWidth: 0
        },
        smooth: false,
        lineStyle: {
          width: 2,
          color: '#ff624a'
        },
        data: yData4
      },
      {
        name: '同比率',
        type: 'line',
        yAxisIndex: 1,
        symbolSize: 0,
        symbol: 'emptyCircle',
        itemStyle: {
          color: '#7dfe4e',
          borderRadius: '100%',
          borderWidth: 0
        },
        smooth: false,
        lineStyle: {
          width: 2,
          color: '#7dfe4e'
        },
        data: yData5
      },
      {
        name: '环比率',
        type: 'line',
        yAxisIndex: 1,
        symbolSize: 0,
        symbol: 'emptyCircle',
        itemStyle: {
          color: '#831eb6',
          borderRadius: '100%',
          borderWidth: 0
        },
        smooth: false,
        lineStyle: {
          width: 2,
          color: '#831eb6'
        },
        data: yData6
      }
    ]
  };
  return option;
};
// 缺陷对标统计
export const histogram1 = (data, color) => {
  let xData = data.map(v => v.areaName);
  let yData1 = data.map(v => v.num);
  let yData2 = data.map(v => v.processed);
  let yData3 = data.map(v => v.pending);
  let yData4 = data.map(v => v.rate.replace('%', ''));
  // let yData5 = data.map(v => v.failureRate);
  // let yData6 = data.map(v => v.timelyProcessingRate);
  let option = {
    backgroundColor: 'transparent',
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    legend: {
      // data: ['总数', '已完成', '待处理', '消缺率', '故障率', '及时处理率'],
      data: ['总数', '已完成', '待处理', '消缺率'],
      x: 'center',
      textStyle: {
        color: '#7f7f7f'
      },
      itemWidth: 10,
      itemHeight: 10,
      itemGap: 25
    },
    grid: {
      top: '30%',
      left: '3%',
      right: '3%',
      bottom: '0%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: xData,
        axisLine: {
          show: true,
          lineStyle: {
            color: '#E4E7ED',
            width: 1,
            type: 'solid'
          }
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          show: true,
          color: '#7f7f7f'
        }
      }
    ],
    yAxis: [
      {
        name: '单位：个',
        nameTextStyle: {
          color: '#7f7f7f'
          // padding: [0, 10, 0, 0]
        },
        type: 'value',
        min: 0,
        minInterval: 1,
        axisLabel: {
          color: '#7f7f7f',
          fontSize: fontSize(14)
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: false,
          lineStyle: {
            color: '#E4E7ED',
            width: 1,
            type: 'solid'
          }
        },
        splitLine: {
          lineStyle: {
            color: '#E4E7ED'
          }
        }
      },
      {
        name: '单位：%',
        nameTextStyle: {
          color: '#7f7f7f'
          // padding: [0, 0, 0, 35]
        },
        type: 'value',
        splitLine: {
          show: false
        },
        min: 0,
        max: 100,
        interval: 20,
        axisLabel: {
          interval: 0,
          color: '#7f7f7f',
          fontSize: 10,
          formatter: '{value}'
        }
      }
    ],
    series: [
      {
        name: '总数',
        type: 'bar',
        data: yData1,
        barWidth: 10, //柱子宽度
        barGap: 0.5, //柱子之间间距
        itemStyle: {
          color: color[0]
        }
      },
      {
        name: '已完成',
        type: 'bar',
        data: yData2,
        barWidth: 10,
        barGap: 0.5,
        itemStyle: {
          color: color[1]
        }
      },
      {
        name: '待处理',
        type: 'bar',
        data: yData3,
        barWidth: 10,
        barGap: 0.5,
        itemStyle: {
          color: color[2]
        }
      },
      {
        name: '消缺率',
        type: 'line',
        yAxisIndex: 1,
        symbolSize: 0,
        symbol: 'emptyCircle',
        itemStyle: {
          color: color[3],
          borderRadius: '100%',
          borderWidth: 0
        },
        smooth: false,
        lineStyle: {
          width: 2,
          color: color[3]
        },
        data: yData4
      }
      // {
      //   name: '故障率',
      //   type: 'line',
      //   yAxisIndex: 1,
      //   symbolSize: 0,
      //   symbol: 'emptyCircle',
      //   itemStyle: {
      //     color: color[4],
      //     borderRadius: '100%',
      //     borderWidth: 0
      //   },
      //   smooth: false,
      //   lineStyle: {
      //     width: 2,
      //     color: color[4]
      //   },
      //   data: yData5
      // },
      // {
      //   name: '及时处理率',
      //   type: 'line',
      //   yAxisIndex: 1,
      //   symbolSize: 0,
      //   symbol: 'emptyCircle',
      //   itemStyle: {
      //     color: color[5],
      //     borderRadius: '100%',
      //     borderWidth: 0
      //   },
      //   smooth: false,
      //   lineStyle: {
      //     width: 2,
      //     color: color[5]
      //   },
      //   data: yData6
      // }
    ]
  };
  return option;
};
