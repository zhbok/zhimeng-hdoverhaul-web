import request from '@/utils/request';

export default {
  //根据id查询附件列表
  queryFileListById(data) {
    return request.post('/fileUploadPublic/queryFileList', data);
  },
  //根据id保存附件
  saveFileToTabById(data) {
    return request.post('/fileUploadPublic/saveFileToTab', data);
  },
  //根据id删除附件
  delFileById(data) {
    return request.get(
      '/fileUploadPublic/deleteFile?id=' + data.id + '&path=' + data.path
    );
  },
  //根据输入值和参数类型模糊搜索
  getDictionaryList(data) {
    return request.post('/tPublicDictionary/dictionaryList', data);
  },
  // 获取区域列表
  getRegionList() {
    return request.post('/region/list');
  },
  // 获取片区列表
  getAreaList() {
    return request.post('/area/list');
  },
  //获取场站列表
  getStationList() {
    return request.post('/station/list');
  },
  // 获取班组列表
  getTeamList() {
    return request.post('/team/list');
  },
  //获取设备列表
  getDeviceList() {
    return request.post('/device/list');
  },
  // 获取用户列表
  getUserList() {
    return request.post('/user/list');
  },
  // 查询一个用户
  queryOneUserApi(data) {
    return request.post('/user/queryOneUser', data);
  },
  // 获取该用户所属组织结构
  getAllOrgIdByUserId(data) {
    return request.post('/userLogin/getAllOrgIdByUserId', data);
  },
  // 查看用户信息和菜单权限
  getUserInfoAndMenus(data) {
    return request.post('/userLogin/getUserInfoAndMenus', data);
  },
  // 根据角色id查询用户
  queryByRoleId(data) {
    return request.post('/getUserByRole/queryByRoleId', data);
  },
  // 根据工单号查找两票工作负责人
  queryPlanVoteLeader(data) {
    return request.post('/planVote/queryPlanVote', data);
  },
  // 添加用户组织
  addUserOrg(data) {
    return request.post('/userOrg/addUserOrg', data);
  },
  // 获取用户组织
  queryUserOrgByUserId(data) {
    return request.post('/userOrg/queryUserOrgByUserId', data);
  },
  // 区域（根据code检索）
  getregionId(data) {
    return request.post('/region/getListByPid', data);
  },
  // 公司（根据code检索）
  getCompanyId(data) {
    return request.post('/company/getListByPid', data);
  },
  // 片区（根据code检索）
  getAreaId(data) {
    return request.post('/area/getListByPid', data);
  },
  // 场站（根据code检索）
  getStationsId(data) {
    return request.post('/station/getListByPid', data);
  },
  // 查看所有角色信息
  getRoleList(data) {
    return request.post('/publicinfo/roleList', data);
  },
  // 备品备件查询(新)
  getNewProductList(data) {
    return request({
      url: '/SpareParts/queryList',
      data: data,
      method: 'post'
    });
  },
  // 备品备件提交审批(新)
  getSyncOutApply(data) {
    return request({
      url: '/SpareParts/syncOutApply',
      data: data,
      method: 'post'
    });
  },
  /**
   * 检修知识库缺陷数据字段--获取缺陷数据字典缺陷类型
   * @returns 
   */
  queryDefectType() {
    console.log()
    return request.post(
      '/tKnowledgeBaseDict/queryDefectType'
    );
  },
  /**
  * 检修知识库缺陷数据字段--获取缺陷数据字典缺陷等级
  * @returns 
  */
  queryDefectGrade() {
    console.log()
    return request.post(
      '/tKnowledgeBaseDict/queryDefectGrade'
    );
  },


};
