// 操作票拟办代办
import request from '@/utils/request';

export default {
  getDefKey(data) {
    return request.post('/testActivity/getDefKey', data);
  },
  // 开启流程
  //获取流程tocken /testActivity/startProcess
  startProcess(data) {
    return request.post('/testActivity/startProcess', data);
  },
  approveFlowStart(tocken, data) {
    return request({
      url: '/approveFlow/start',
      method: 'post',
      data
    });
  },
  // /processApprove/getPending
  getPending(data) {
    return request.post('/processApprove/getPending', data);
  },
  // 流程动作-同意
  agree(tocken, data) {
    return request({
      url: '/approveFlow/agree',
      method: 'post',
      data
    });
  },
  ///approveFlow/reject
  reject(data) {
    return request({
      url: '/approveFlow/reject',
      method: 'post',
      data
    });
  },
  ///activity/flowChart
  flowChart(data) {
    return request.post('/activity/flowChart', data);
  },
  getAuditRecord(data) {
    return request.post('/processApprove/getAuditRecord', data);
  },
  // 挂起
  suspendProcess(data) {
    return request.post('/approveFlow/suspendProcess', data);
  },
  // 恢复
  recoverProcess(data) {
    return request.post('/approveFlow/recoverProcess', data);
  },
  async submit() {
    const data =
      (
        await this.getPending({
          ticketCode: data
        })
      )?.data ?? {};
  },
  //获取流程历史
  // getFlowHistory(data) {
  //   return request({
  //     url: '/approveFlow/flowHistory',
  //     method: 'post',
  //     data
  //   });
  // },
  // 查询流程任务
  queryTaskIdInfo(data) {
    return request.post(
      '/tUpDefectWorkOrder/queryTaskIdInfo?workOrderNum=' + data.id
    );
  },
  //获取流程历史
  getFlowHistory(data) {
    return request({
      url: '/approveFlow/localFlowHistory',
      method: 'post',
      data
    });
  },

  // 启动
  start(data) {
    return request({
      url: '/approveFlow/start',
      method: 'post',
      data
    });
  },
  handleAgree(data) {
    return request({
      url: '/approveFlow/agree',
      method: 'post',
      data
    });
  },

  // 程序处理驳回
  isStoragReject(data) {
    return request({
      url: '/tUpSpcTask/reject',
      method: 'post',
      data
    });
  },
  // 驳回到初始节点
  rejectStarter(data) {
    return request({
      url: '/approveFlow/rejectStarter',
      method: 'post',
      data
    });
  },
  // 
  //检修知识库驳回
  schemeLibraryReject(data) {
    return request({
      url: '/SchemeLibrary/reject',
      method: 'post',
      data,
    })
  },
  //检修工序卡驳回
  processCardReject(data) {
    return request({
      url: '/processCard/reject',
      method: 'post',
      data,
    })
  },
  //作业指导书
  operationInstructionReject(data) {
    return request({
      url: '/operationInstruction/reject',
      method: 'post',
      data,
    })
  },
  //设备异动导入后数据调用接口
  importUpdateState(data) {
    return request({
      url: '/transactionManage/importUpdateState',
      method: 'post',
      data,
    })
  },
  // 根据组织编号查询生计部编号
  allorgGetCodeByOrgId(data) {
    return request({
      url: '/allorg/getCodeByOrgId',
      method: 'post',
      data,
    })
  },
};
