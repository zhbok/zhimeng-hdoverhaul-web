import request from '@/utils/request';
// 首页接口
export default {
  // 计划性检修
  planOverhaulStat(data) {
    return request.post('/firstpage/planOverhaulStat', data);
  },
  // 非计划性检修状态统计-缺陷状态统计
  defectStatusStat(data) {
    return request.post('/firstpage/defectStatusStat', data);
  },
  // 非计划性检修状态统计-特殊检修状态统计
  specialOverhualStatusStat(data) {
    return request.post('/firstpage/specialOverhualStatusStat', data);
  },
  // 工作台
  workTable(data) {
    return request.post('/firstpage/workTable', data);
  },
  // 工作台
  workTableDays(data) {
    return request.post('/firstpage/workTableDays', data);
  },
  // 非计划性检修类型统计-特殊检修类型
  specialOverhualTypeStat(data) {
    return request.post('/firstpage/specialOverhualTypeStat', data);
  },
  // 非计划性检修类型统计-缺陷类型
  defectTypeStat(data) {
    return request.post('/firstpage/defectTypeStat', data);
  },
  // 特殊检修汇总
  specialOverhualStat(data) {
    return request.post('/firstpage/specialOverhualStat', data);
  },
  // 区域基本信息
  regionBasicInfo(data) {
    return request.post('/firstpage/regionBasicInfo', data);
  }
};
