import request from '@/utils/request2';

export default {
  //查询两票审批列表
  getList4WorkOrder(data) {
    return request({
      url: '/api/workTicket/getPage4WorkOrder',
      data: data,
      method: 'post'
    });
  },
  //查询两票中操作票审批列表
  getListOptionWorkOrder(data) {
    return request({
      url: '/api/opticket/getList4WorkOrder',
      data: data,
      method: 'post'
    });
  },
  //查询两票审批日志
  getflowHistory(data) {
    return request({
      url: '/api/workflow/flowHistory',
      data: data,
      method: 'post'
    });
  },
};
