// 作业指导书
import request from '@/utils/request';


    export default{
      //  查询作业指导书列表
      queryOperationInstruction(data) {
      return request.post('/operationInstruction/queryOperationInstruction', data)
    },
    //  添加作业指导书
    addOperationInstruction(data){
      return request.post('/operationInstruction/addOperationInstruction', data)
    },
    //  作业指导书全部导出
    allExportOperationInstruction(data){
      return request.post(' /operationInstruction/allExportOperationInstruction', data)
    },
    //  删除作业指导书
    delOperationInstructionById(data){
      return request.post('/operationInstruction/delOperationInstructionById', data)
    },
    // 删除作业指导书准备工作
    delPreparatoryWork(data){
      return request.post('/operationInstruction/delPreparatoryWork', data)
    },
    // 删除作业指导书防范措施
    delSafeMeasure(data){
      return request.post('/operationInstruction/delSafeMeasure', data)
    },
    // 删除作业指导书人员要求 
    delPersonnelRequire(data){
      return request.post('/operationInstruction/delPersonnelRequire', data)
    },
     // 删除作业指导书备品备件
     delSparePart(data){
      return request.post('/operationInstruction/delSparePart', data)
    },
    // 删除作业指导书检修工序卡
    delProcessCard(data){
      return request.post('/operationInstruction/delProcessCard', data)
    },
    //  作业指导书导出
    exportOperationInstruction(data){
      return request.post('/operationInstruction/exportOperationInstruction', data)
    },
    // 导入作业指导书
    importExcel(data){
      return request.post('/operationInstruction/importExcel', data)
    },
    // 作业指导书导入模板下载
    importExcelTemplate(data){
      return request.post('/operationInstruction/importExcelTemplate', data)
    },
    // 作业指导书详情
    operationInstructionDetails(data){
      return request.post('/operationInstruction/operationInstructionDetails', data)
    },
    // 编辑指导书
    upDateOperationInstruction(data){
      return request.post('/operationInstruction/upDateOperationInstruction', data)
    },
    // 作业指导书名称模糊查询
    queryOperationInstructionName(data){
      return request.post('/operationInstruction/queryOperationInstructionName', data)
    },
    // 作业指导书适用范围模糊查询
    queryUseScope(data){
      return request.post('/operationInstruction/queryUseScope', data)
    },
    }
   
  

