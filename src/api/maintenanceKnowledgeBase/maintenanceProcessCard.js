// 检修工序卡
import request from '@/utils/request';


    export default{
     //班组选项
     teamOption(data) {
      return request.post('/team/list', data)
    },
      // 查询检修工序卡列表
      queryProcessCard(data) {
      return request.post('/processCard/queryProcessCard', data)
    },
    //  添加检修工序卡
    addProcessCard(data){
      return request.post('/processCard/addProcessCard', data)
    },
    //  检修工序卡全部导出
    allExportProcessCard(data){
      return request.post('/processCard/allExportProcessCard', data)
    },
    //  删除检修工序卡
    delProcessCardById(data){
      return request.post('/processCard/delProcessCardById', data)
    },
    //  删除检修工序卡内容
    delProCardDetails(data){
      return request.post('/processCard/delProCardDetails', data)
    },
    //  检修工序卡导出
    ExportProcessCard(data){
      return request.post('/processCard/ExportProcessCard', data)
    },
    // 导入检修工序卡
    importExcel(data){
      return request.post('/processCard/importExcel', data)
    },
    // 检修工序卡导入模板下载
    importExcelTemplate(data){
      return request.post('/processCard/importExcelTemplate', data)
    },
    // 检修工序卡详情
    processCardDetails(data){
      return request.post('/processCard/processCardDetails', data)
    },
    // 修改指导书
    upDateProcessCard(data){
      return request.post('/processCard/upDateProcessCard', data)
    },
     //检修工序卡工序卡名称模糊查询
     queryProcessCardName(data){
      return request.post('/processCard/queryProcessCardName', data)
    },
    //检修工序卡设备类型模糊查询
    queryProcessCardDeviceType(data){
      return request.post('/processCard/queryProcessCardDeviceType', data)
    },
    //检修工序卡-启用
    open(data){
      return request.post('/processCard/open', data)
    },
    //检修工序卡-停用
    close(data){
      return request.post('/processCard/close', data)
    },

    }
   
  

