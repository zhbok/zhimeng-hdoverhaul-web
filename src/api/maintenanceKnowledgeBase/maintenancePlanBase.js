import request from '@/utils/request';

export default {
  //检修方案库分页列表
  getSchemeLibraryPage(data) {
    return request.post('/SchemeLibrary/querySchemeLibrary', data);
  },
  //设备类型模糊搜索
  queryApplyEquipmentType(data) {
    return request.post('/SchemeLibrary/queryApplyEquipmentType', data);
  },
  //检修项目模糊搜索
  queryOverhaulProject(data) {
    return request.post('/SchemeLibrary/queryOverhaulProject', data);
  },
  //检修方案库新增及启动流程
  schemeLibraryStart(data) {
    return request.post('/SchemeLibraryFlow/SchemeLibraryStart', data);
  },
  //检修方案库审批同意流程
  schemeLibraryAgree(data) {
    return request.post('/SchemeLibraryFlow/SchemeLibraryAgree', data);
  },
  //检修方案库审批驳回上一节点流程
  schemeLibraryReject(data) {
    return request.post('/SchemeLibraryFlow/SchemeLibraryReject', data);
  },
  //检修方案库审批驳回初始节点流程
  schemeLibraryRejectStarter(data) {
    return request.post('/SchemeLibraryFlow/SchemeLibraryRejectStarter', data);
  },
  //检修方案库编辑
  editSchemeLibrary(data) {
    return request.post('/SchemeLibrary/updateSchemeLibrary', data);
  },
  //检修方案库删除
  delSchemeLibrary(data) {
    return request.post('/SchemeLibrary/deleteSchemeLibraryById', data);
  },
  //检修方案库详情
  getSchemeLibraryDetail(data) {
    return request.post('/SchemeLibrary/detailsSchemeLibrary?id=' + data.id);
  },
  //检修方案库启用
  openSchemeLibrary(data) {
    return request.post('/SchemeLibrary/openSchemeLibrary', data);
  },
  //检修方案库停用
  closeSchemeLibrary(data) {
    return request.post('/SchemeLibrary/closeSchemeLibrary', data);
  },
  //检修方案库查询备品备件
  querySparePartPage(data) {
    return request.post('/sparePart/querySparePart', data);
  },
  //检修方案库删除备品备件
  delSparePart(data) {
    return request.post('/SchemeLibrary/delSparePart', data);
  },
  //检修方案库查询工序卡
  queryProcessCardPage(data) {
    return request.post('/processCard/queryProcessCard', data);
  },
  //检修方案库删除工序卡
  delProcessCard(data) {
    return request.post('/SchemeLibrary/delProcessCard', data);
  },
  //检修方案库查询作业指导书
  queryOperationInstructionPage(data) {
    return request.post(
      '/operationInstruction/queryOperationInstruction',
      data
    );
  },
  //检修方案库删除作业指导书
  delOperationInstruction(data) {
    return request.post('/SchemeLibrary/delOperationInstruction', data);
  },
  //检修方案库作业指导书分页查询及所属备品备件和工序卡
  queryOperationInstructionChild(data) {
    return request.post(
      '/operationInstruction/queryOperationInstructionChild',
      data
    );
  },

  // 备品备件查询
  getProductList(data) {
    return request({
      url: '/SpareParts/getList',
      data: data,
      method: 'post'
    });
  },
  //检修方案库查询工序卡---新接口
  queryOpenProcessCard(data) {
    return request.post('/processCard/queryOpenProcessCard', data);
  },

  //检修方案库查询作业指导书--新增接口
  queryOpenOperationInstruction(data) {
    return request.post(
      '/operationInstruction/queryOpenOperationInstruction',
      data
    );
  },

};

