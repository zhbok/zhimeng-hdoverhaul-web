import request from '@/utils/request';

export default {
  //缺陷字典类型分页列表
  getDictPage(data) {
    return request.post(
      '/tKnowledgeBaseDict/tKnowledgeBaseDictSelectAll',
      data
    );
  },
  //缺陷字典类型新增
  createDictPage(data) {
    return request.post('/tKnowledgeBaseDict/insertTKnowledgeBaseDict', data);
  },
  //缺陷字典类型编辑
  editDictPage(data) {
    return request.post('/tKnowledgeBaseDict/updateTKnowledgeBaseDict', data);
  },
  //缺陷字典类型删除
  delDictPage(data) {
    return request.post('/tKnowledgeBaseDict/deleteTKnowledgeBaseDict', data);
  },
  //缺陷字典名称模糊查询
  queryDictName(data) {
    return request.post('/tKnowledgeBaseDict/queryDictName', data);
  },
  //缺陷字典编码模糊查询
  queryDictCode(data) {
    return request.post('/tKnowledgeBaseDict/queryDictCode', data);
  },
  //缺陷字典类型回收站分页列表
  getDictRestorePage(data) {
    return request.post(
      '/tKnowledgeBaseDictRecycle/tKnowledgeBaseDictSelectAll',
      data
    );
  },
  //缺陷字典类型回收站删除
  delDictRestorePage(data) {
    return request.post('/tKnowledgeBaseDictRecycle/deleteThoroughById', data);
  },
  //缺陷字典类型回收站恢复
  restoreDict(data) {
    return request.post('/tKnowledgeBaseDictRecycle/repealRecycle', data);
  },
  //缺陷字典值分页列表
  getDictValuePage(data) {
    return request.post(
      '/tKnowledgeBaseDict/tKnowledgeBaseDictValueList',
      data
    );
  },
  //缺陷字典值新增
  createDictValuePage(data) {
    return request.post(
      '/tKnowledgeBaseDict/insertTKnowledgeBaseDictValue',
      data
    );
  },
  //缺陷字典值编辑
  editDictValuePage(data) {
    return request.post(
      '/tKnowledgeBaseDict/updateTKnowledgeBaseDictValue',
      data
    );
  },
  //缺陷字典值删除
  delDictValuePage(data) {
    return request.post(
      '/tKnowledgeBaseDict/deleteTKnowledgeBaseDictValue',
      data
    );
  },
  //缺陷字典值模糊查询
  queryDictValueName(data) {
    return request.post('/tKnowledgeBaseDict/queryDictValueName', data);
  },
  //缺陷字典值模糊查询
  queryDictValueCode(data) {
    return request.post('/tKnowledgeBaseDict/queryDictValueCode', data);
  },
};
