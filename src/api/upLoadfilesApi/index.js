import request from '@/utils/request';
export function upExcel(data) {
  return request({
    url: 'planOverhaulProject/importExcel',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data
  })
}