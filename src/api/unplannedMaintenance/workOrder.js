import request from '@/utils/request';

export default {
  //缺陷工单分页列表查询
  getDefectWorkOrderPage(data) {
    return request.post('/tUpDefectWorkOrder/defectWorkOrderList', data);
  },
  //缺陷工单提交
  commitWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/commit', data);
  },
  //缺陷工单详情
  getWorkOrderDetail(data) {
    return request.post(
      '/tUpDefectWorkOrder/defectWorkOrderDetail?workOrderNumber=' + data.id
    );
  },
  //缺陷工单关联工作
  relevanceBookAndCard(data) {
    return request.post('/tUpDefectWorkOrder/relevanceBookAndCard', data);
  },
  //缺陷工单查询指导书版本编号列表
  queryBookVersionIdList(data) {
    return request.post('/operationInstruction/queryBookVersionIdList', data);
  },
  //缺陷工单根据版本编号查询作业指导书详情
  queryBookByVersionId(data) {
    return request.post(
      '/operationInstruction/queryBookByVersionId?versionId=' + data.id
    );
  },
  //缺陷工单查询工序卡号列表
  queryCardCodeList(data) {
    return request.post('/processCard/queryCardCodeList', data);
  },
  //缺陷工单根据版本编号查询工序卡详情
  queryProcessCardByCode(data) {
    return request.post(
      '/processCard/queryProcessCardByCode?cardCode=' + data.id
    );
  },
  //处理缺陷工单
  handleWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/handle', data);
  },
  //挂起缺陷工单
  hangUpWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/hangUp', data);
  },
  //挂起审批缺陷工单
  approveWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/hangUpApprove', data);
  },
  //激活缺陷工单
  activateWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/activate?id=' + data.id);
  },
  //缺陷工单检修交代
  overhaulExplainOrder(data) {
    return request.post('/tUpDefectWorkOrder/overhaulExplain', data);
  },
  //缺陷工单验收
  checkWorkOrder(data) {
    return request.post('/tUpDefectWorkOrder/acceptanceCheck', data);
  },
  // 备品备件关联编码回写
  updateSpareCode(data) {
    return request.post('/sparePartMain/updateSpareCode', data);
  },
  //查看历史记录
  queryHistoryRecord(data) {
    return request.get(
      '/tUpDefectWorkOrder/historyRecord?pageNum=' +
      data.pageNum +
      '&pageSize=' +
      data.pageSize +
      '&deviceCodeId=' +
      data.deviceCodeId
    );
  }, //查看历史记录详情
  historyRecordDetail(data) {
    console.log(data)
    return request.post(
      '/tUpDefectWorkOrder/historyRecordDetail?workOrderNumber=' +
      data.workOrderNumber
    );
  },
  // 缺陷工单转特殊检修
  toSpecialOverhaul(data) {
    return request({
      url:
        'tUpDefectWorkOrder/toSpecialOverhaul?workOrderNum=' +
        data.workOrderNum +
        '&nextUserId=' +
        data.nextUserId +
        '&nextUserName=' +
        data.nextUserName,
      method: 'post'
    });
  }
};
