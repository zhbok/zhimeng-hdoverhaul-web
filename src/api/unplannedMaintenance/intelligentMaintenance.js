import request from '@/utils/request';

export default {
  // 获取区域列表
  getAreaList(data) {
    return request.post('/area/list', data);
  },
  //获取场站列表
  getStationList() {
    return request.post('/station/list');
  },
  // 缺陷数量统计
  getDefectNumberStat(data) {
    return request.post('/homepage/defectNumberStat', data);
  },

  // 缺陷类型统计:新的
  getNewDefectType(data) {
    return request.post('/homepage/defectType', data);
  },

  // 缺陷类型统计
  getDefectTypeStat(data) {
    return request.post('/homepage/defectTypeStat', data);
  },

  // 计划性检修进度统计
  getPlanOverhaulStat(data) {
    return request.post('/homepage/planOverhaulStat', data);
  },

  // 区域基本信息
  getRegionBasicMessage(data) {
    return request.post('/homepage/regionBasicMessage', data);
  },

  // 与我相关
  getRelateToMe(data) {
    return request.post('/homepage/relateToMe', data);
  },

  // 特殊检修数量统计
  getSpcNumberStat(data) {
    return request.post('/homepage/spcOverhaulNumberStat', data);
  },

  // 特殊检修类型统计
  getSpcTypeStat(data) {
    return request.post('/homepage/spcOverhaulTypeStat', data);
  },

  // 工作台
  getWorkbench(data) {
    return request.post('/homepage/workbench', data);
  },
  // 特殊检修类型统计
  getspcOverhaulType(data) {
    return request.post('/homepage/spcOverhaulType', data);
  },
  // 工作台待办事项
  workbenchToDoList(data) {
    return request.post('/homepage/todo', data);
  },
  // 工作台已办事项
  workbenchDoneList(data) {
    return request.post('/homepage/done', data);
  },
  // 工作台待办事项
  workbenchFinishList(data) {
    return request.post('/homepage/finish', data);
  }
};
