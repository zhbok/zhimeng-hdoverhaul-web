import request from '@/utils/request';

export default {
  //缺陷任务分页列表查询
  getDefectTaskPage(data) {
    return request.post('/tUpDefectTask/defectTaskList', data);
  },
  //缺陷任务删除
  delDefectTask(data) {
    return request.post('/tUpDefectTask/delDefectTask', data);
  },
  //新增、编辑缺陷任务
  createDefectTask(data) {
    return request.post('/tUpDefectTask/addDefectTask', data);
  },
  //缺陷任务详情
  getDefectTaskDetail(data) {
    return request.post('/tUpDefectTask/defectTaskDetail?id=' + data.id);
  },
  //缺陷任务待提交详情
  getDefectEditDetail(data) {
    return request.post('/tUpDefectTask/defectEditDetail?id=' + data.id);
  },
  //是否转特殊检修
  toSpecialOverhaul(data) {
    return request.post('/tUpDefectTask/toSpecialOverhaul?id=' + data.id + '&nextUserId=' + data.nextUserId + '&nextUserName=' + data.nextUserName);
  },
  //生成工单
  defectTaskToWorkOrder(data) {
    return request.post('/tUpDefectTask/defectTaskToWorkOrder', data);
  },
  //提交任务
  submitDefect(data) {
    return request.post('/tUpDefectTask/defectSubmit', data);
  },
  //获取工单编号列表
  getWorkNumList(data) {
    return request.post('/tUpDefectTask/relevanceWorkOrderList?deviceId=' + data.deviceId);
  },
  //关联工单
  relateWorkOrder(data) {
    return request.post('/tUpDefectTask/relevanceWorkOrder', data);
  }
};
