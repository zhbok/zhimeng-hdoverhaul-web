import request from '@/utils/request';

export default {
  // 查询缺陷状态统计列表
  getDefectStatusList(data) {
    return request.post('/tUpDefectStatistics/defectStatusList', data);
  },
  // 缺陷月度统计-柱状图
  getEefectMonthChart(data) {
    return request.post('/tUpDefectStatistics/defectMonthChart', data);
  },
  // 缺陷状态统计列表-全部导出
  exportDefectStatus(data) {
    return request.post('/tUpDefectStatistics/allExportDefectStatus', data);
  },
  // 获取下拉数据接口
  getDictList(data) {
    return request.post('/tPublicDictionary/dictionaryList', data);
  },
  // 缺陷对标统计-柱状图
  getDefectBenchChart(data) {
    return request.post('/tUpDefectStatistics/defectBenchChart', data);
  },
};
