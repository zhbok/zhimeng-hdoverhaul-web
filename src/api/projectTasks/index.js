import request from '@/utils/request';
//计划性检修
export default {
   //获取上次检修时间
   getLastOverhaulTime(data) {
    return request.post('/planOverhaulProject/getLastOverhaulTime1', data);
  },
  //检修计划列表
  planOverhaulProjectList(data) {
    return request.post('/planOverhaulProject/list', data);
  },
  //检修项目列表
  overhaulList(data) {
    return request.post('/planOverhaulProject/overhaulList', data);
  },
  //检修计划保存
  planOverhaulProjectAdd(data) {
    return request.post('/planOverhaulProject/add', data);
  },
  //检修计划修改
  planOverhaulProjectEdit(data) {
    return request.post('/planOverhaulProject/edit', data);
  },
  //检修计划删除
  planOverhaulProjectDeleteById(data) {
    return request.post('/planOverhaulProject/deleteById', data);
  },
  //检修计划提交
  commitProjectPlan(data) {
    return request.post('/planOverhaulProject/commitProjectPlan', data);
  },
  //检修计划详情
  planOverhaulProjectDetail(data) {
    return request.post('/planOverhaulProject/detail?id=' + data);
  },
  //检修计划变更
  planOverhaulProjectChangeRequest(data) {
    return request.post('/planOverhaulProject/changeRequest', data);
  },
  //检修任务保存
  planOverhaulProjectTaskSave(data) {
    return request.post('/planOverhaulProjectTask/save', data);
  },
  //检修任务修改
  updatePlanOverhaulProjectTask(data) {
    return request.post(
      '/planOverhaulProjectTask/updatePlanOverhaulProjectTask',
      data
    );
  },
  //检修任务生成工单
  planOverhaulProjectTaskToWorkOrder(data) {
    return request.post(
      '/planOverhaulProjectTask/planOverhaulProjectTaskToWorkOrder',
      data
    );
  },
  //检修任务列表
  queryPlanOverhaulProjectTaskList(data) {
    return request.post(
      '/planOverhaulProjectTask/queryPlanOverhaulProjectTaskList',
      data
    );
  },
  //检修任务明细
  queryPlanOverhaulProjectTask(data) {
    return request.post(
      '/planOverhaulProjectTask/queryPlanOverhaulProjectTask',
      data
    );
  },
  //检修任务删除
  removeByIds(data) {
    return request.post('/planOverhaulProjectTask/removeByIds', data);
  },
  //检修任务导出
  exportPlanOverhaulProjectTask(data) {
    return request.post(
      '/planOverhaulProjectTask/exportPlanOverhaulProjectTask',
      data
    );
  },
  // 根据设备类型下的所有设备生成检修计划
  planOverhaulProjectGeneratePlan(data) {
    return request.post('/planOverhaulProject/generatePlan', data);
  },
  //片区
  areaList(data) {
    return request.post('/area/list', data);
  },
  // 场站
  stationList(data) {
    return request.post('/station/list', data);
  },
  // 设备名称
  deviceList(data) {
    return request.post('/device/list', data);
  },
  // 设备类型
  deviceTypeList(data) {
    return request.post('/deviceType/list', data);
  },
  // 用户
  userList(data) {
    return request.post('/user/list', data);
  },
  //检修工单列表
  planOverhaulItemWorkorderList(data) {
    return request.post(
      '/planOverhaulItemWorkOrder/planOverhaulItemWorkorderList',
      data
    );
  },
  //检修工单明细
  queryPlanOverhaulItemWorkOrder(data) {
    return request.post(
      '/planOverhaulItemWorkOrder/queryPlanOverhaulItemWorkOrder',
      data
    );
  },
  //检修工单关联工作
  relevanceBookAndCard(data) {
    return request.post(
      '/planOverhaulItemWorkOrder/relevanceBookAndCard',
      data
    );
  },
  //检修工单提交
  planOverhaulItemWorkOrderCommit(data) {
    return request.post('/planOverhaulItemWorkOrder/commit', data);
  },
  //检修工单处理
  planOverhaulItemWorkOrderHandle(data) {
    return request.post('/planOverhaulItemWorkOrder/handle', data);
  },
  //检修工单检修交代
  overhaulExplain(data) {
    return request.post('/planOverhaulItemWorkOrder/overhaulExplain', data);
  },
  //检修工单验收
  acceptanceCheck(data) {
    return request.post('/planOverhaulItemWorkOrder/acceptanceCheck', data);
  },
  //设备异动列表
  queryTransactionManageList(data) {
    return request.post('/transactionManage/queryTransactionManageList', data);
  },
  //设备异动新增及启动流程
  facilityTransactionStart(data) {
    return request.post('/facilityTransactionFlow/facilityTransactionStart', data);
  },
  //设备异动审批同意流程
  facilityTransactionAgree(data) {
    return request.post('/facilityTransactionFlow/facilityTransactionAgree', data);
  },
  //设备异动审批驳回上一节点流程
  facilityTransactionReject(data) {
    return request.post('/facilityTransactionFlow/facilityTransactionReject', data);
  },
  //设备异动审批驳回初始节点流程
  facilityTransactionRejectStarter(data) {
    return request.post('/facilityTransactionFlow/facilityTransactionRejectStarter', data);
  },
  //设备异动修改
  upDateTransactionManage(data) {
    return request.post('/transactionManage/upDateTransactionManage', data);
  },
  //设备异动删除
  delTransactionManageById(data) {
    return request.post('/transactionManage/delTransactionManageById', data);
  },
  //设备异动明细
  transactionManageDetails(data) {
    return request.post('/transactionManage/details', data);
  },
  //设备异动生成报告
  reportTransactionManage(data) {
    return request.post('/transactionManage/reportTransactionManage', data);
  },
};
