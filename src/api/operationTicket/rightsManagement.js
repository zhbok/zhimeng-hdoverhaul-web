// 操作票模块 api
import request from '@/utils/request';

/**
 * 权限板块 api
 * @returns promise
 */
export function useRightsManagementApi() {
  return {
    // 获取表格数据 示例
    postListData: data =>
      request({
        url: '/authority/getAuthorityList',
        method: 'post',
        data
      }),
    // new Promise((resolve, reject) => {
    //   resolve({
    //     data: {
    //       dataList: [
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: true,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '胡东请',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx集控中心',
    //           name: '王小虎',
    //           operator: true,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         }, {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         }, {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         }, {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //         {
    //           affiliatedUnit: 'xxx片区',
    //           name: '王小虎',
    //           operator: false,
    //           firstGuardian: true,
    //           secondGuardian: true,
    //           dutyLeader: true,
    //           shiftSupervisor: true,
    //           must: true,
    //           spotCheck: true
    //         },
    //       ],
    //       totalCount: 4
    //     }
    //   });
    // }),
    saveCheckBox: data =>
      request({
        url: '/authority/modify',
        method: 'post',
        data
      }),

    //获取用户信息
    getUserInfo: () =>
      request({
        url: '/user/queryOneUser',
        method: 'post',
        data: {}
      }),
  };
}
