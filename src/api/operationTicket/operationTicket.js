// 操作票模块 api
import request from '@/utils/request';

/**
 * 拟办板块 api
 * @returns promise
 */
export function useProposedApi() {
  return {
    // 获取表格数据 示例
    postListData: data =>
      // request({
      //   url: '/mock/getList',
      //   method: 'post',
      //   data
      // })
      new Promise((resolve, reject) => {
        resolve({
          data: {
            dataList: [
              {
                date: '2016-05-02',
                name: '王小虎',
                address: '上海市普陀区金沙江路 1518 弄'
              },
              {
                date: '2016-05-04',
                name: '王小虎',
                address: '上海市普陀区金沙江路 1517 弄'
              },
              {
                date: '2016-05-01',
                name: '王小虎',
                address: '上海市普陀区金沙江路 1519 弄'
              },
              {
                date: '2016-05-03',
                name: '王小虎',
                address: '上海市普陀区金沙江路 1516 弄'
              }
            ],
            totalCount: 4
          }
        });
      })
  };
}


