// 操作票模块 api
import request from '@/utils/request';

/**
 * 票库板块 api
 * @returns promise
 */
export function getTicketApi() {
    return {
        // 获取表格数据 示例
        gettTreeData: data =>
            request({
                url: '/dangerPoint/dangerPointTree',
                method: 'post',
                data
            }),
        // 获取表格数据 示例
        postListData: data =>
            request({
                url: '/bank/listPage',
                method: 'post',
                data
            }),

        // 提交表单数据
        //审核
        auditTicket: data =>
            request({
                url: '/bank/audit',
                method: 'post',
                data
            }),
        addTicket: data =>
            request({
                url: '/bank/add',
                method: 'post',
                data
            }),
        // 修改票库
        updateTicket: data =>
            request({
                url: '/bank/update',
                method: 'post',
                data
            }),
        //删除
        deleteTicket: data =>
            request({
                url: '/bank/delete',
                method: 'post',
                data
            }),
        //查看详情
        detailTicket: data =>
            request({
                url: '/bank/detail',
                method: 'post',
                data
            }),

        // 获取危险点数据
        getPeril: data =>
            request({
                url: '/dangerPoint/pageByDetail',
                method: 'post',
                data
            }),

        // 新增控制措施
        addPeril: data =>
            request({
                url: '/dangerPoint/saveDetail',
                method: 'post',
                data
            }),

        // 删除危险点详情
        deleteDetailById: data =>
            request({
                url: '/dangerPoint/deleteDetailById',
                method: 'post',
                data
            }),
        // 删除危险点详情
        deleteById: data =>
            request({
                url: '/dangerPoint/deleteById',
                method: 'post',
                data
            }),
        // 新增控制措施
        addTreeNdoes: data =>
            request({
                url: '/dangerPoint/saveDangerPoint',
                method: 'post',
                data
            }),



    };
}