// 操作票模块 api
import request from '@/utils/request';

/**
 * 操作票统计 api
 * @returns promise
 */
export function getStatisticsApi() {
  return {
    getList: data =>
      request({
        url: '/votestat/OperateTicketStat',
        method: 'post',
        data
      })
  };
}
