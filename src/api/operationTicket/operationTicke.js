// 操作票拟办代办
import request from '@/utils/request';


    export default{
      // 典型票
    ticketBankApi(data) {
      return request.post('/bank/listPage', data)
    },

    // 查询操作票待办拟办历史列表
    historyListApi(data){
      return request.post('/operationvote/queryHistoryList', data)
    },


    // 查询操作票待办拟办列表
    listTableApi(data){
      return request.post('/operationvote/queryList', data)
    },

    // 删除操作票待办拟办
    deleteApi(data){
      return request.post('/operationvote/delete', data)
    },

    // 作废操作票待办拟办
    cancelApi(data){
      return request.post('/operationvote/cancel', data)
    },
    
    // 操作票已办待办导出
    exportApi(data){
      return request.get('/operationvote/exportOperationExcel',  {params: data})
    },

    // 新增操作票待办拟办
    addApi(data){
      return request.post('/operationvote/insert', data)
    },

    // 查看操作票待办拟办详情
    descApi(data){
      return request.post('/operationvote/queryDetail', data)
    },

    // 修改操作票待办拟办
    updateApi(data){
      return request.post('/operationvote/update', data)
    },

    // 用户
    userApi(data){
      return request.post('/user/list', data)
    },

    // 班组列表
    teamApi(data){
      return request.post('/team/list', data)
    },

    // 场站列表
    stationApi(data){
      return request.post('/station/list', data)
    },

    // 操作流程记录查询'
    getRecordsApi(data){
      return request.post('/processRecord/getRecords', data)
    } ,
      // 是否能新建拟办待办
      isUsersByCategory(data) {
        return request.post('/authority/getEnabledUsersByCategory', data);
      },

      //评价
      appraiseApi(data){
        return request.post('/appraise/add', data);
      },

      //典型票详情
      dxpdescApi(data){
         return request.post('/bank/detail',data)
      },

      // 查询一个用户
      queryOneUserApi(data){
        return request.post('/user/queryOneUser',data)
      },
      
      //转典型票
      importBankApi(data){
        return request.post('/bank/importBank',data)
      },
        // 危险点详情
      pageByDetailAPi(data) {
        return request.post('/dangerPoint/pageByDetail', data);
      }
     
    }
   
  

