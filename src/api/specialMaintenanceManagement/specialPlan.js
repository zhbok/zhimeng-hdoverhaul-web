// 特殊检修计划
import request from '@/utils/request';
export default {
    //查询特殊检修计划列表
    spcSpcPlanList(data) {
        return request.post('/tUpSpcSpllitPlan/spcSpcPlanList', data)
    },
    // 特殊计划-全部导出
    allExportSpcPlan(data) {
        return request.post('/tUpSpcSpllitPlan/allExportSpcPlan', data)
    },
    // 特殊计划-导出
    exportSpcPlan(data) {
        return request.post('/tUpSpcSpllitPlan/exportSpcPlan', data)
    },
    // 特殊子计划验收功能
    acceptSpcSubPlan(data) {
        return request.post('/tUpSpcSpllitPlan/acceptSpcSubPlan', data)
    },
    // 特殊子计划验收界面展示
    acceptSpcSubPlanDetail(data) {
        return request.post('/tUpSpcSpllitPlan/acceptSpcSubPlanDetail', data)
    },
    // 删除拆分子计划
    delSplitPlan(data) {
        return request.post('/tUpSpcSpllitPlan/delSplitPlan', data)
    },
    // 特殊子计划任务生成工单
    spcSubPlanWorkOrder(data) {
        return request.post('/tUpSpcSpllitPlan/spcSubPlanWorkOrder', data)
    },
    // 特殊子计划任务查询工单
    spcSubPlanWorkOrderdetailHis(data) {
        return request.post('/tUpSpcSpllitPlan/spcSubPlanWorkOrderdetailHis', data)
    },
    // 保存拆分子计划
    splitPlan(data) {
        return request.post('/tUpSpcSpllitPlan/splitPlan', data)
    }, 
    // 查询子计划详情
    splitPlanDetail(data) {
        return request.post('/tUpSpcSpllitPlan/splitPlanDetail', data)
    },
     // 编辑子计划
     editSplitPlan(data) {
        return request.post('/tUpSpcSpllitPlan/editSplitPlan', data)
    },
    // 查询验收子计划数据
    acceptSplitPlanDetail(data) {
        return request.post('/tUpSpcSpllitPlan/acceptSplitPlanDetail', data)
    },
    // 保存验收子计划数据
    saveAcceptSplitPlan(data) {
        return request.post('/tUpSpcSpllitPlan/saveAcceptSplitPlan', data)
    },
}



