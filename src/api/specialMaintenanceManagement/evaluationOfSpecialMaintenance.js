// 特殊检修结果评估
import request from '@/utils/request';
export default {
    //查询特殊检修结果评估列表
    spcResultEvalList(data) {
        return request.post('/TUpSpcResultEval/spcResultEvalList', data)
    },
    //特殊检修结果评估-导出
    exportSpcWorkOrder(data) {
        return request.post('/TUpSpcResultEval/exportSpcWorkOrder', data)
    },
    //特殊检修结果评估全部导出
    allExportSpcWorkOrder(data) {
        return request.post('/TUpSpcResultEval/allExportSpcWorkOrder', data)
    },
}



