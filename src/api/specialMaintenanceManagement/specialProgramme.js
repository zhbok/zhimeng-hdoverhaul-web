// 特殊检修方案
import request from '@/utils/request';
export default {
  //查询特殊缺陷任务列表
  spcTaskList(data) {
    return request.post('/tUpSpcTask/spcTaskList', data)
  },
  // 特殊缺陷任务-导出
  exportSpcTask(data) {
    return request.post('/tUpSpcTask/exportSpcTask', data)
  },
  // 特殊缺陷任务-全部导出
  allExportSpcTask(data) {
    return request.post('/tUpSpcTask/allExportSpcTask', data)
  },
  //智能决策 指导书
  addBook(data) {
    return request.post('/tUpSpcTask/addBook', data)
  },
  //智能决策 工序卡
  addCard(data) {
    return request.post('/tUpSpcTask/addCard', data)
  },
  //智能决策 方案库
  addScheme(data) {
    return request.post('/tUpSpcTask/addScheme', data)
  },
  //智能决策 备品备件
  addSparePart(data) {
    return request.post('/tUpSpcTask/addSparePart', data)
  },
  //查询特殊缺陷智能决策信息
  queryInteDecTask(data) {
    return request.post('/tUpSpcTask/queryInteDecTask', data)
  },
  //保存特殊缺陷智能决策信息
  operateInteDecTask(data) {
    return request.post('/tUpSpcTask/operateInteDecTask', data)
  },
  //删除智能决策信息方案库
  delSchemeLibrary(data) {
    return request.post('/tUpSpcTask/delSchemeLibrary', data)
  },
  //删除智能决策信息指导书
  delBook(data) {
    return request.post('/tUpSpcTask/delBook', data)
  },
  //删除智能决策信息工序卡
  delProcessCard(data) {
    return request.post('/tUpSpcTask/delProcessCard', data)
  },
  //删除智能决策信息备品备件
  delSparePart(data) {
    return request.post('/tUpSpcTask/delSparePart', data)
  },
  // 获取智能决策弹窗中方案名称
  getSchemeName() {
    return request.post('/tUpSpcTask/createScheme')
  },
  // ****************金风智能决策----一键推送***************************

  /**
   * 工单匹配推送
   * @param {*} data 
   *        device_cls	string	  必须		设备类型	
            device_parts	string	必须		适用部位	
            platform_type	string	必须		平台，型号	
            fault_name	string	  必须		缺陷类型	
            fault_reason	string	必须		原因分析	
   * @returns 
   */
  getWorkPushId(data) {
    console.log(data)
    return request.post(
      '/workOrder/push/scheme/id',
      data
    );
  },
  /**
   * 基本信息列表
   * @param {*} data 
   *        base_source_id	string	非必须		请求ID     
   * @returns 
   */
  getWorkBaseInfo(data) {
    console.log(data)
    return request.post(
      '/workOrder/push/base/list', data
    );
  },


  /**
   * 获取检修工具卡详情
   * @param {*} data 
   *        base_source_id	string	必须		请求ID 
   *        process_card_base_source_id  string	必须		工序卡ID  
   * @returns 
   */
  getProcessCardDetail(data) {
    console.log(data)
    return request.post(
      '/workOrder/push/ProcessCard/detail', data
    );
  },

  /**
   * 获取方案库详情
   * @param {*} data 
   *        base_source_id	string	必须		请求ID     
   * @returns 
   */
  getSolutionLibraryDetail(data) {
    console.log(data)
    return request.post(
      '/workOrder/push/scheme/detail', data
    );
  },
}



