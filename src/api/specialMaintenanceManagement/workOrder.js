import request from '@/utils/request';

export default {
  //特殊检修工单分页列表查询
  getSpecialWorkOrderPage(data) {
    return request.post('/tUpSpcWorkOrder/spcWorkOrderList', data);
  },
  //特殊检修工单详情
  getWpcWorkOrderDetail(data) {
    return request.post('/tUpSpcWorkOrder/spcWorkOrderDetail', data);
  },
  //特殊检修工单提交
  commitWpcWorkOrder(data) {
    return request.post('/tUpSpcWorkOrder/commit', data);
  },
  //特殊检修工单关联工作
  relevanceAddSpcBookAndCard(data) {
    return request.post('/tUpSpcWorkOrder/relevanceAddSpcBookAndCard', data);
  },
  //特殊检修工单处理
  handleWpcWorkOrder(data) {
    return request.post('/tUpSpcWorkOrder/handle', data);
  },
  //特殊检修工单检修交代
  overhaulExplainWpcWorkOrder(data) {
    return request.post('/tUpSpcWorkOrder/overhaulExplain', data);
  },
  //特殊检修工单验收
  checkWpcWorkOrder(data) {
    return request.post('/tUpSpcWorkOrder/acceptanceCheck', data);
  },
  //特殊检修工单评估
  evaluateWpcWorkOrder(data) {
    return request.post('/tUpSpcWorkOrder/assessResult', data);
  },
};
