// 计划性检修结果评估
import request from '@/utils/request';
export default {
    //查询计划性检修结果评估列表
    planOverhaulResult(data) {
        return request.post('/planOverhaulResult/list', data)
    },
    //计划性检修结果评估-导出
    exportPlanOverhaulResultAppraise(data) {
        return request.post('/planOverhaulResult/exportPlanOverhaulResultAppraise', data)
    },
    //计划性检修结果评估全部导出
    exportAllPlanOverhaulResultAppraise(data) {
        return request.post('/planOverhaulResult/exportAllPlanOverhaulResultAppraise', data)
    },
}



