import request from '@/utils/request'

export default {
  planOverhaulProjectTaskSave (data) {
    return request.post('/planOverhaulProjectTask/save', data)
  },
// 上传附件
upload (data) {
    return request.post('/fileUploadPublic/upload', data)
  },
// 保存附件
saveFileToTab (data) {
    return request.post('/fileUploadPublic/saveFileToTab', data)
  },
// 查询附件列表
queryFileList (data) {
    return request.post('/fileUploadPublic/queryFileList', data)
  },
   //根据id删除附件
   delFileById(data) {
    return request.get(
      '/fileUploadPublic/deleteFile?id=' + data.id + '&path=' + data.path
    );
  },
// 下载 查看
downLoad (data) {
    return request.get('/fileUploadPublic/downLoad', {params: data})
  },
}
