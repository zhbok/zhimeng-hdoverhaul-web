import request from '@/utils/request';

export default {
  getEquipmentType: params => {
    return request({
      url: '/assetment/getEquipmentType',
      method: 'get',
      params,
    });
  },
  getEquipCodeList: data => {
    return request({
      url: '/assetment/searchList',
      method: 'post',
      data: {
        pageNum: 1,
        pageSize: 100, //设备资产限制100
        ...data
      }
    })
  },
  // 基于设备类型批量查询缺陷部位列表
  searchListTreeByType: data => {
    return request({
      url: '/assetment/searchListTreeByType',
      method: 'post',
      data: {
        pageNum: 1,
        pageSize: 100, //设备资产限制100
        ...data
      }
    })
  },
  // 基于设备类型查询缺陷部位列表的nodeTree
  searchListNodeTree: data => {
    return request({
      url: '/assetment/searchNodeTree',
      method: 'post',
      data: {
        pageNum: 1,
        pageSize: 100, //设备资产限制100
        ...data
      }
    })
  },
  searchListTree: (data) => {
    return request({
      url: '/assetment/searchListTree',
      method: 'post',
      data: data,
    })
  },
  getDepartsList: (data) => {
    return request({
      url: '/tKnowledgeBaseDict/queryPartsByDeviceTypeName',
      method: 'post',
      data: data,
    })
  }
};
