import request from '@/utils/request1';
import requestHZ from '@/utils/requestHZ';
import requestSpare from '@/utils/requestSpare';

export default {
  //查询设备类型列表
  getEquipmentTypeList(data) {
    return request({
      url: '/api/equipmentInstance/equipmentType/tree',
      params: data,
      method: 'get'
    });
  },
  //根据设备类型-工器具查询
  searchToolsList(data) {
    return request({
      url: '/equipmentInstance/searchList',
      data: data,
      method: 'post'
    });
  },
  // 备品备件查询
  getProductList(data) {
    return requestSpare({
      url: '/exchange/api/productDetail/productDetailListForApi',
      data: data,
      method: 'post'
    });
  },
    // 备品备件提交审批
    productSubmit(data) {
      return requestSpare({
        // url: '/exchange/api/apply/syncOutApply',
        url: 'api/hdwms/apply/syncOutApply',
        data: data,
        method: 'post'
      });
    },
    // 请单状态查询
    queryOutboundStatus(data) {
      return requestSpare({
        url: '/exchange/api/apply/queryOutboundStatus',
        params: data,
        method: 'get'
      });
    },
    // 杭州备品备件提交审批后状态
    productSubmitOverStatus(data) {
      return requestSpare({
        url: '/exchange/api/apply/queryOutboundStatus',
        data: data,
        method: 'post'
      });
    },
    // 新查询工器具接口
    getToolsList(data) {
      return requestHZ({
        url: '/api/equipmentTool/list',
        data: data,
        method: 'post'
      });
    },
};
