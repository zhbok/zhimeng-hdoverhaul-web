import { Message } from 'element-ui';

function errorValidate(fileds, refs) {
    let str = [];
    for (let key in fileds) {
        fileds[key].map((item) => {
            str.push(item.message);
        });
        let dom = refs[Object.keys(fileds)[0]];
        console.log(dom.length, typeof(dom))
        if (dom) {
            if (Object.prototype.toString.call(dom) !== '[object Object]') {
                dom = dom[0];
                break; //结束语句并跳出语句，进行下个语句执行
            }
            // 定位代码
            dom.$el.scrollIntoView({
                block: 'center',
                behavior: 'smooth'
            });
        }
    }
    // 页面提示未通过校验字段项，并以逗号分隔
    Message({
        message: str[0],
        type: 'error',
    })
}
export { errorValidate }