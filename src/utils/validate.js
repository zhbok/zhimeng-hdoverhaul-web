/**
 * @param {string} value
 * @returns {Boolean}
 * 字母或数字
 */
export function validateLettersOrNum(rule, value, callback) {
  const reg = /^[0-9A-Za-z]+$/;
  if (value === '' || value === undefined || value == null) {
    callback();
  } else {
    if (!reg.test(value) && value !== '') {
      callback(new Error('请输入字母或数字'));
    } else {
      callback();
    }
  }
}
/**
 * @param {string} value
 * @returns {Boolean}
 * 整数
 */
export function validateInteger(rule, value, callback) {
  const reg = /^\+?[0-9]\d*$/;
  if (value === '' || value === undefined || value == null) {
    callback();
  } else {
    if (!reg.test(value) && value !== '') {
      callback(new Error('请输入大于等于0整数'));
    } else {
      callback();
    }
  }
}

/**
 * @param {string} value
 * @returns {Boolean}
 * 保留两位小数
 */
export function validateTwoDecimal(rule, value, callback) {
  const reg = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
  if (value === '' || value === undefined || value == null) {
    callback();
  } else {
    if (!reg.test(value)) {
      callback(new Error('请输入正数（最多两位小数）'));
    } else {
      callback();
    }
  }
}
