const dayjs = require('dayjs');
import request from './request';
import { Message } from 'element-ui';

export const HEADER_TOKEN_KEY = 'X-Authorization-Access_token';
export const SAC_OTHER_SYSTEM = 'Sac-Other-System';
export const SAC_UN = 'Sac-Un';

/**
 * 获取uuid
 */
export function getUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16) | 0 : 'r&0x3' | '0x8').toString(
      16
    );
  });
}

/**
 * 下载上传的文件
 * @param {*} url 下载文件的url
 * @param {*} fileName 下载文件名
 * @param {*} data 入参
 * @param {*} method 请求方式
 */
export function downloadFile(url, fileName, data, method = 'post') {
  const parmas = {
    url,
    method,
    responseType: 'blob',
    transformResponse: function (data) {
      return data;
    }
  };
  if (method.toLocaleLowerCase() === 'get') {
    parmas.parmas = data;
  } else {
    parmas.data = data;
  }
  request(parmas)
    .then(res => {
      if (res instanceof Blob) {
        let url = window.URL.createObjectURL(res);
        let link = document.createElement('a');
        link.style.display = 'none';
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      } else {
        Message.error('下载文件失败');
      }
    })
    .catch(e => {
      if (e instanceof Blob) {
        let reader = new FileReader();
        reader.onload = function () {
          let jsonObj = JSON.parse(reader.result);
          Message.error((jsonObj || {}).errorMessage || '下载文件失败');
        };
        reader.readAsText(e);
      } else {
        Message.error('下载文件失败');
      }
    });
}

/**
 * 日期格式化
 * @param {*} date
 * @param {*} formatString
 * @returns
 */
export function formatDate(date, formatString) {
  return dayjs(date).format(formatString);
}

/**
 * 浏览器分辨率转换大小
 * @param {*} val
 * @param {*} initWidth
 * @param {*} method
 * @returns
 */
export function fontSize(val, initWidth = 1920) {
  let nowClientWidth =
    document.documentElement.clientWidth || window.screenWidth;
  if(document.documentElement.clientWidth >= 1920 || window.screenWidth >= 1920){
    nowClientWidth = 1920
  }
  if (!nowClientWidth) return 16;
  return val * (nowClientWidth / initWidth);
}

//数组去重
export function unique(arr) {
  if (!Array.isArray(arr)) {
    console.log('type error!');
    return;
  }
  let res = [];
  for (let i = 0; i < arr.length; i++) {
    if (res.indexOf(arr[i]) === -1) {
      res.push(arr[i]);
    }
  }
  return res;
}

/**
 * 下载上传的文件
 * @param {*} url 下载文件的url
 * @param {*} fileName 下载文件名
 * @param {*} data 入参
 * @param {*} method 请求方式
 */
export function downloadPDFFile(url, fileName, data, method = 'post') {
  const parmas = {
    url,
    method,
    responseType: 'blob',
    transformResponse: function (data) {
      return data;
    }
  };
  if (method.toLocaleLowerCase() === 'get') {
    parmas.parmas = data;
  } else {
    parmas.data = data;
  }
  request(parmas)
    .then(res => {
      if (res instanceof Blob) {
        let url = window.URL.createObjectURL(res);
        let link = document.createElement('a');
        link.style.display = 'none';
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      } else {
        Message.error('下载文件失败');
      }
    })
    .catch(e => {
      if (e instanceof Blob) {
        let reader = new FileReader();
        reader.onload = function () {
          let jsonObj = JSON.parse(reader.result);
          Message.error((jsonObj || {}).errorMessage || '下载文件失败');
        };
        reader.readAsText(e);
      } else {
        Message.error('下载文件失败');
      }
    });
}
//  * 排序比较
//  * @param {string} propertyName 排序的属性名
//  * @param {string} sort ascending(升序)/descending(降序)
//  * @return {function}
export function compare(propertyName, sort) {
  // 判断是否为数字
  function isNumberV(val) {
    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg =
      /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)) {
      return true;
    } else {
      return false;
    }
  }

  return function (obj1, obj2) {
    let value1 = obj1[propertyName];
    let value2 = obj2[propertyName];
    // 数字类型的比较
    if (isNumberV(value1) || isNumberV(value2)) {
      if (sort === 'ascending') {
        return value1 - value2;
      } else {
        return value2 - value1;
      }
    }
    // 布尔值的比较：利用减法-转化true 和 false
    // true => 1   false ⇒ 0
    // true-false => 1  false-true => -1
    // 下面方法是按照先false后true的顺序排序，如果想先true后false，调整value1-value2 和  value2 - value1的顺序即可
    else if (_.isBoolean(value1) && _.isBoolean(value2)) {
      if (sort === 'ascending') {
        return value1 - value2;
      } else {
        return value2 - value1;
      }
    }
    // 字符比较使用localeCompare()
    else {
      const res = value1.localeCompare(value2, 'zh');
      return sort === 'ascending' ? res : -res;
    }
  };
}

/**
 * 对比两个数组，筛选出arr中不在brr中的项
 * @param {array} arr 需对比的数组
 * @param {array} brr 被对比的数组
 * @param {string} field 对比使用的字段
 * @returns {array}
 */
export function differenceByField(arr, brr, field) {
  const newArr = arr.filter(item => {
    return !brr.some(bItem => {
      return JSON.stringify(bItem[field]) === JSON.stringify(item[field])
    })
  })
  return newArr
}


//验证不通过回滚到不通过的第一个位置
export function scrollToFirstError(that) {
  that.$nextTick(() => {
    let isError = document.getElementsByClassName('is-error')
    isError[0].scrollIntoView({
      block: 'center',
      behavior: 'smooth',
    })
  })
}

// 节流
export function throttle(func, delay) {
  var delay = delay || 1000;
  var previousDate = new Date();
  var previous = previousDate.getTime();  // 初始化一个时间，也作为高频率事件判断事件间隔的变量，通过闭包进行保存。
  
  return function(args) {
      var context = this;
      var nowDate = new Date();
      var now = nowDate.getTime();
      if (now - previous >= delay) {  // 如果本次触发和上次触发的时间间隔超过设定的时间
          func.call(context, args);  // 就执行事件处理函数 （eventHandler）
          previous = now;  // 然后将本次的触发时间，作为下次触发事件的参考时间。
      }
  }
}

//判断两个数组中是否有重复元素
export function findCommonElements(arr1, arr2) { 
  // Create an empty object 
  let obj = {}; 

  // Loop through the first array 
  for (let i = 0; i < arr1.length; i++) { 

      // Check if element from first array 
      // already exist in object or not 
      if (!obj[arr1[i]]) { 

          // If it doesn't exist assign the 
          // properties equals to the  
          // elements in the array 
          let element = arr1[i]; 
          obj[element] = true; 
      } 
  } 

  // Loop through the second array 
  for (let j = 0; j < arr2.length; j++) { 

      // Check elements from second array exist 
      // in the created object or not 
      if (obj[arr2[j]]) { 
          return true; 
      } 
  } 
  return false; 
}