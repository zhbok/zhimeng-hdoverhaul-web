import publicApi from '@/api/publicApi';
import { getToken } from '@/utils/auth';
import { Message } from 'element-ui';

const cache = (function () {
  let _cache = {};
  return {
    getCache(key) {
      return _cache[key];
    },
    setCache(key, data) {
      _cache[key] = data;
    }
  };
})();

export const getRoleInfo = async (key, allUser, sign) => {
  let roleList = cache.getCache('roleList');
  if (!roleList || !roleList.length) {
    let val = await publicApi.getRoleList({ pageNo: 1, limit: 1000 });
    if (val.code === 200) {
      roleList = val.data;
      cache.setCache('roleList', roleList);
    }
  }
  let obj = JSON.parse(localStorage.getItem('vuex_hdoverhaul'));
  let employee = obj?.user?.userLoginInfo?.employee ?? {};
  if (roleList && roleList.length > 0) {
    let role = roleList.filter(v => v.name.includes(key))[0];
    if (role && role.id) {
      let list = [];
      const params = { parameters: [{ key: 'roleId', value: role.id }] };
      const res = await publicApi.queryByRoleId(params);
      if (res.code == 200) {
        if (sign == '2' || allUser) {
          list = res?.data ?? [];
        } else {
          list = res.data.filter(v => v.groupID == employee.groupID);
        }
        return list;
      } else {
        return list;
      }
    }
  }
};

// regionId
// companyId
// areaId

export const getUserListByArea = async (key, sign = '1') => {
  let obj = JSON.parse(localStorage.getItem('vuex_hdoverhaul'));
  const { regionId, companyId, areaId, deptId } = obj?.user?.allOrgInfo ?? {
    regionId: null,
    companyId: null,
    areaId: null,
    deptId: null
  };
  const userList = await getRoleInfo(key, !areaId, sign);
  if (companyId) {
    return userList;
  }
  if (deptId) {
    return userList;
  }
  if (areaId) {
    return userList;
  }
  if (!regionId) return userList;
  // if (!companyId || !regionId) { }
  const areaList = await publicApi.getAreaId({
    pid: companyId,
    region_code: regionId
  });

  let resUserList = userList.filter(item => {
    if (
      areaList.data.some(area => {
        return area.areaName == item.orgName;
      })
    ) {
      return item;
    }
  });
  console.log('************************getUserList***************************');
  console.log(resUserList);
  return resUserList;
};
