import axios from 'axios';
import { Message } from 'element-ui';
import { getToken, getSacUn } from './auth';
import { HEADER_TOKEN_KEY, SAC_OTHER_SYSTEM, SAC_UN } from './index';

/**
 * 单文件上传
 * fileList：文件列表
 * url：接口url
 * @returns
 */
const headersCom = {}
headersCom[HEADER_TOKEN_KEY] = getToken()
// headersCom[SAC_OTHER_SYSTEM] = true;
// headersCom[SAC_UN] = getSacUn();
const file = {
  'Content-Type': 'multipart/form-data',
}
const headersFile = { ...headersCom, ...file }

export const uploadFile = ({ fileList, url }) => {
  let formData = new FormData();
  // 将上传的文件放到数据对象中
  formData.append('file', fileList[0].raw);
  formData.append('projectCode', 'sac-file-provider');
  // formData.append(
  //   'teamId',
  //   localStorage.getItem('teamId') ? localStorage.getItem('teamId') : ''
  // )
  // formData.append('platformType', 'pc')
  return new Promise((resolve, reject) => {
    axios
      .post(url, formData, {
        headers: headersFile
      })
      .then(res => {
        if (res.data.code == 200) {
          resolve(res.data);
        } else {
          Message.error({
            message: res.data.message,
            duration: 1500
          });
          resolve(res.data);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
};
/**
 * excel上传
 * fileList：文件列表
 * url：接口url
 * @returns
 */
export const uploadExcel = ({ fileList, url, loginId, loginName }) => {
  let formData = new FormData();
  // 将上传的文件放到数据对象中
  formData.append('file', fileList[0].raw);
  formData.append('loginId', loginId);
  formData.append('loginName', loginName);
  // formData.append('orgId', orgId);
  return new Promise((resolve, reject) => {
    axios
      .post(url, formData, {
        headers: headersFile
      })
      .then(res => {
        if (res.data.code == 200) {
          resolve(res.data);
        } else {
          Message.error({
            message: res.data.message,
            duration: 1500
          });
          resolve(res.data);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
};

/**
 * 文件下载
 * path 文件路径
 * @returns
 */
export const downloadFile = data => {
  return new Promise((resolve, reject) => {
    axios({
      method: data.method,
      url: data.url,
      params: data.params,
      responseType: 'blob',
      headers: headersCom
    })
      .then(res => {
        const link = document.createElement('a'); //创建a标签
        let blob = new Blob([res.data]);
        // 兼容不同浏览器的URL对象
        const url = window.URL || window.webkitURL || window.moxURL;
        link.href = url.createObjectURL(blob);
        link.download = data.fileName; //下载的文件名称
        link.click(); //触发click
        window.URL.revokeObjectURL(url);
        resolve('下载成功');
      })
      .catch(error => {
        reject(error);
      });
  });
};
