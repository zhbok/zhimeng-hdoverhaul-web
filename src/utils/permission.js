import { setRem } from './rem.js';
import store from '../store';
export function initSubAppRouterGuards(router, props = {}) {
  let isExecuted = false;
  router.beforeEach(async (to, from, next) => {
    if (!isExecuted) {
      window.localStorage.removeItem('vuex_hdoverhaul');
      window.localStorage.removeItem('roleList');
      window.localStorage.removeItem('orgInfo');
      // 这里写需要执行一次的逻辑代码
      await store.dispatch(
        'getUserInfoOnce',
        process.env.VUE_APP_BASE_SYSTEMID
      );
      setRem();
      next();
      isExecuted = true;
    } else {
      setRem();
      next(); // 若已经执行过则直接通过next()进入下一个路由
    }
  });
}
