import { Message } from 'element-ui';

const DEFAULT_PAGE_SIZE = 20;

export class TableWidget {
  /**
   * 表格组件
   * @param {function (params: Object) : Promise} loadTableData 表数据获取函数
   * @param {Boolean} paged 是否支持分页
   * @param {Boolean} rowSelection 是否支持行选择
   * @param {String} orderFieldName 默认排序字段
   * @param {Boolean} ascending 默认排序方式（true为正序，false为倒序）
   * @param {String} dateAggregateBy 默认排序字段的日期统计类型
   */
  constructor(
    loadTableData,
    paged,
    rowSelection,
    orderFieldName,
    ascending,
    dateAggregateBy
  ) {
    this.currentRow = null;
    this.loading = false;
    this.oldPage = 0;
    this.currentPage = 1;
    this.oldPageSize = DEFAULT_PAGE_SIZE;
    this.pageSize = DEFAULT_PAGE_SIZE;
    this.totalCount = 0;
    this.dataList = [];
    this.resultInfo = {};
    this.columnMetaList = [];
    this.orderInfo = {
      fieldName: orderFieldName,
      asc: ascending,
      dateAggregateBy: dateAggregateBy
    };
    this.paged = paged;
    this.rowSelection = rowSelection;
    this.loadTableData =
      loadTableData ||
      function () {
        return Promise.resolve();
      };
    this.onCurrentPageChange = this.onCurrentPageChange.bind(this);
    this.onPageSizeChange = this.onPageSizeChange.bind(this);
    this.onSortChange = this.onSortChange.bind(this);
    this.getTableIndex = this.getTableIndex.bind(this);
    this.currentRowChange = this.currentRowChange.bind(this);
  }

  /**
   * 表格分页变化
   * @param {Integer} newCurrentPage 变化后的显示页面
   */
  onCurrentPageChange(newCurrentPage) {
    this.loadTableDataImpl(newCurrentPage, this.pageSize)
      .then(() => {
        this.oldPage = this.currentPage = newCurrentPage;
      })
      .catch(() => {
        this.currentPage = this.oldPage;
      });
  }

  /**
   * 表格分页每页显示数量变化
   * @param {Integer} newPageSize 变化后的每页显示数量
   */
  onPageSizeChange(newPageSize) {
    this.pageSize = newPageSize;
    this.currentPage = 1;
    this.loadTableDataImpl(1, newPageSize)
      .then(() => {
        this.oldPage = this.currentPage;
        this.oldPageSize = this.pageSize;
      })
      .catch(e => {
        this.currentPage = this.oldPage;
        this.pageSize = this.oldPageSize;
      });
  }

  /**
   * 表格排序字段变化
   * @param {String} prop 排序字段的字段名
   * @param {String} order 正序还是倒序
   */
  onSortChange({ prop, order }) {
    this.orderInfo.fieldName = prop;
    this.orderInfo.asc = order === 'ascending';
    this.refreshTable();
  }

  /**
   * 获取每一行的index信息
   * @param {Integer} index 表格在本页位置
   */
  getTableIndex(index) {
    return this.paged
      ? (this.currentPage - 1) * this.pageSize + (index + 1)
      : index + 1;
  }

  /**
   * 当前选中行改变
   * @param {Object} currentRow 当前选中行
   * @param {Object} oldRow 老的选中行
   */
  currentRowChange(currentRow, oldRow) {
    this.currentRow = currentRow;
  }

  clearTable() {
    this.currentRow = null;
    this.oldPage = 0;
    this.currentPage = 1;
    this.totalCount = 0;
    this.dataList = [];
  }

  /**
   * 获取表格数据
   * @param {Integer} pageNum 当前分页
   * @param {Integer} pageSize 每页数量
   * @param {Boolean} reload 是否重新获取数据
   */
  loadTableDataImpl(pageNum, pageSize, reload = false) {
    return new Promise((resolve, reject) => {
      if (typeof this.loadTableData !== 'function') {
        reject();
      } else {
        // 如果pageSize和pageNum没有变化，并且不强制刷新
        if (
          this.paged &&
          !reload &&
          this.oldPage === pageNum &&
          this.oldPageSize === pageSize
        ) {
          resolve();
        } else {
          let params = {};
          if (this.orderInfo.fieldName != null)
            params.orderParam = [this.orderInfo];
          if (this.paged) {
            params.pageParam = {
              pageNum,
              pageSize
            };
          }
          this.loading = true;
          this.loadTableData(params)
            .then(tableData => {
              const { dataList, totalCount, columnMetaList, resultInfo } =
                tableData;
              this.dataList = dataList;
              this.columnMetaList = columnMetaList || [];
              this.resultInfo = resultInfo || {};
              this.totalCount = totalCount;
              this.loading = false;
              resolve();
            })
            .catch(e => {
              this.loading = false;
              reject(e);
            });
        }
      }
    });
  }

  /**
   * 刷新表格数据
   * @param {Boolean} research 是否按照新的查询条件重新查询（调用verify函数）
   * @param {Integer} pageNum 当前页面
   */
  refreshTable(research = false, pageNum = undefined, showMsg = false) {
    if (Number.isInteger(pageNum) && pageNum !== this.currentPage) {
      this.loadTableDataImpl(pageNum, this.pageSize, research)
        .then(res => {
          this.oldPage = this.currentPage = pageNum;
          if (research && showMsg) Message.success('查询成功');
        })
        .catch(e => {
          this.currentPage = this.oldPage;
        });
    } else {
      this.loadTableDataImpl(this.currentPage, this.pageSize, true).catch(
        e => {}
      );
    }
  }
}
