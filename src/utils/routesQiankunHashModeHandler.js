import QiankunCt from '@/Qiankun';
import Layout from '@/layout';
export function pathJoin(...args) {
  return args.join('/').replace(/\/+/g, '/');
}
export function autoPath(path) {
  if (path === '*') {
    return path;
  }
  // console.log(window.__MICRO_ROUTE_PREFIX__);
  return window.__POWERED_BY_QIANKUN__
    ? pathJoin(window.__MICRO_ROUTE_PREFIX__, path)
    : path;
}

export function routesQiankunHashModeHandler(routes, root) {
  const res = [];
  routes.forEach(route => {
    const tmp = { ...route };
    if (tmp.children) {
      tmp.children = routesQiankunHashModeHandler(tmp.children, false);
      if (root && tmp.component === Layout) {
        // console.log('eqwwwwwwwwwwwwwwwwwwwwwww');
        tmp.component = QiankunCt;
      }
    }
    if (root && tmp.path) {
      tmp.path = autoPath(tmp.path);
    }
    if (tmp.redirect) {
      tmp.redirect = autoPath(tmp.redirect);
    }
    res.push(tmp);
  });

  return res;
}
