import Cookies from 'js-cookie'
import sacJson from '../../public/sac.json'

const nzApplicationName = sacJson.qiankun.name
const JOIN_STR = '__'

export function getStorageKey(name) {
  return name
  // return window.__POWERED_BY_QIANKUN__
  //   ? `${nzApplicationName}${JOIN_STR}${name}`
  //   : name
}

export function setCookie(name, value, options) {
  const _name = getStorageKey(name)
  return Cookies.set(_name, value, options)
}

export function getCookie(name) {
  if (typeof name === 'string') {
    const _name = getStorageKey(name)
    return Cookies.get(_name)
  }
  return Cookies.get()
}

export function removeCookie(name, options) {
  const _name = getStorageKey(name)
  return Cookies.remove(_name, options)
}

export function setLocalStorage(name, value) {
  const _name = getStorageKey(name)
  return localStorage.setItem(_name, value)
}

export function getLocalStorage(name) {
  const _name = getStorageKey(name)
  return localStorage.getItem(_name)
}

export function removeLocalStorage(name) {
  const _name = getStorageKey(name)
  return localStorage.removeItem(_name)
}

export function setSessionStorage(name, value) {
  const _name = getStorageKey(name)
  return sessionStorage.setItem(_name, value)
}

export function getSessionStorage(name) {
  const _name = getStorageKey(name)
  return sessionStorage.getItem(_name)
}

export function removeSessionStorage(name) {
  const _name = getStorageKey(name)
  return sessionStorage.removeItem(_name)
}
