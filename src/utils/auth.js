import { getCookie, removeCookie, setCookie } from './storage';

// const TokenKey = 'main-ibps-3.5.0-LC.RELEASE-token';
const TokenKey = 'ibps-3.5.0-LC.RELEASE-token';
const UuidKey='ibps-3.5.0-LC.RELEASE-uuid'

export function getToken() {
  return getCookie(TokenKey);
}

export function setToken(token) {
  return setCookie(TokenKey, token);
}

export function removeToken() {
  return removeCookie(TokenKey);
}

export function getSacUn() {
  const getUuid = getCookie(UuidKey)
  const ibpsRelease =JSON.parse(localStorage.getItem('ibps-3.5.0-LC.RELEASE')) 
  const getAccount = ibpsRelease.sys.user[getUuid].user.info.user?.account || ''
  return getAccount;
}