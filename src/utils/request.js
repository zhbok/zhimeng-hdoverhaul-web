import axios from 'axios';
import { MessageBox, Message } from 'element-ui';
import store from '@/store';
import { getToken, getSacUn } from './auth';
import { globalState } from '@/plugins/globalState';
import { HEADER_TOKEN_KEY, SAC_OTHER_SYSTEM, SAC_UN } from './index';

const EXPIRE_STATE = 6020301;
const VERIFY_STATE = [6020201, 6020202, 6020109, 6020160];
const ALLOW_STATES = [200, 2, 103];
const UNAUTHORIZED_CODE = 401

window.axiosCancel = [];

// create an axios instance
const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  baseURL: window.__POWERED_BY_QIANKUN__
    ? process.env.VUE_APP_BASE_API
    : '/api', // url = base url + request url
  // baseURL:'api',
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000 // request timeout
});
// request interceptor
service.interceptors.request.use(
  config => {
    config.cancelToken = new axios.CancelToken((cancel) => {
      window.axiosCancel.push({
        cancel,
      });
    });
    const token = getToken();
    if (token && !config.noToken) {
      config.headers[HEADER_TOKEN_KEY] = token;
    }
    // 本地联调时不要
    // config.headers[SAC_OTHER_SYSTEM] = true;
    // config.headers[SAC_UN] = getSacUn();
    if (config.method == 'post') {
      if (config.data) {
        config.data.operationId = store.state.user.userInfo.userId;
      }
    } else {
      if (config.params) {
        config.params.operationId = store.state.user.userInfo.userId;
      } else {
        config.params = {
          operationId: store.state.user.userInfo.userId
        };
      }
    }

    // 本地联调
    // config.headers['HEADER_TOKEN_KEY'] = '4b17760dbd6d43d1bc0912058b7ce1af'
    // config.headers['X-Authorization-Access_token'] = '4b17760dbd6d43d1bc0912058b7ce1af'
    // dkyld:1183167425265274880    yrpqbzcy:1182454629233856512
    config.data = Object.assign(config.data || {}, { operationId: '1182454629233856512' })
    config.params = Object.assign(config.params || {}, { operationId: '1182454629233856512' })
    return config;
  },
  error => {
    // do something with request error
    console.log(error); // for debug
    return Promise.reject(error);
  }
);
/**
 * qiankun方式子应用与基座通信刷新token
 */
const stateMessage = (action = 'refresh') => {
  return new Promise(resolve => {
    globalState.state.setGlobalState({
      api: action + ':token',
      from: 'apps_sac_microqiankun'
    });
    globalState.state.onGlobalStateChange(state => {
      if (state.api === 'set:token') {
        // setToken(state.token); // 如果直接取南自的cookie的token就别自己维护token了
        resolve(state.token);
      }
    });
  });
};
// qiankun子应用token刷新机制
// 正在重试标记
let isRefreshing = false;
// 待重试组合
let queue = [];
/**
 * qiankun子应用token刷新机制
 * @param response 接口响应体
 * @returns
 */
export const refreshToken = async (response, action = 'refresh') => {
  const config = response.config;
  if (!isRefreshing) {
    isRefreshing = true;
    try {
      try {
        const token = await stateMessage(action); // 非法token平台跳转登录页，返回接口自身response
        if (token === null) {
          return response;
        }
        config.headers[HEADER_TOKEN_KEY] = token;
        queue.forEach(cb => cb(token));
        queue = [];
        return await service(config);
      } catch (res) {
        console.error('refreshtoken error =>', res);
      }
    } finally {
      isRefreshing = false;
    }
  }
  return new Promise(resolve => {
    queue.push(token => {
      config.headers[HEADER_TOKEN_KEY] = token;
      resolve(service(config));
    });
  });
};
service.interceptors.response.use(
  async response => {
    if (response.data.code) {
      const { code } = response.data;
      const config = response.config; // 允许的状态码
      const allowStates = [
        ...new Set(ALLOW_STATES.concat(config.allowStates || []))
      ];
      if (allowStates.includes(code)) {
        return Promise.resolve(response.data);
      } else if (code === EXPIRE_STATE) {
        return refreshToken(response, 'refresh');
      } else if (VERIFY_STATE.includes(code)) {
        return refreshToken(response, 'verify');
      } else if (code === UNAUTHORIZED_CODE) {
        // 401状态取消其他请求
        let cancelArr = [...window.axiosCancel];
        cancelArr.forEach((ele, index) => {
          ele.cancel("return");
          delete window.axiosCancel[index];
        });
        Message({
          message: response.data.message || response.data.cause || 'Error',
          type: 'error',
          duration: 5 * 1000
        })
        return;
      } else {
        Message({
          message: response.data.message || response.data.cause || 'Error',
          type: 'error',
          duration: 5 * 1000
        });
        return Promise.reject(
          response.data.message || response.data.cause || 'Error'
        );
      }
    } else {
      return Promise.resolve(response.data);
    }
  },
  error => {
    const { response = {} } = error || {};
    const { status } = response || {};
    if (status === UNAUTHORIZED_CODE) {
      return refreshToken(response);
    }
    return Promise.reject(error);
  }
);

export default service;
