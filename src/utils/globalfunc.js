import { Message } from 'element-ui';
import store from '@/store';

// 获取按钮权限
const btnPermissions=(name = '', fun)=>{
  // 获取vuex存储所有按钮权限
  let menusArr = store.state.user.menus;
  let flag = false; 
  // 不传按钮别名的状态默认不加权限
  if(!name){
    flag = true
  } else{
    flag = menusArr.includes(name);
  }
  if(flag){
    fun()
  } else {
    Message({
      message: '您没有权限，请先联系管理员授权',
      type: 'warning',
      duration:  3 * 1000
    })
  }
};

export default {
  btnPermissions
}