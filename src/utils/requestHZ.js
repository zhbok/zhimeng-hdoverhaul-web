import axios from 'axios';
import { MessageBox, Message } from 'element-ui';
import store from '@/store';
import { getToken, getSacUn } from './auth';
import { HEADER_TOKEN_KEY, SAC_OTHER_SYSTEM, SAC_UN } from './index';

// create an axios instance
const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // baseURL: window.__POWERED_BY_QIANKUN__ ? process.env.VUE_APP_UPLOAD_API : 'api', // url = base url + request url
  // baseURL: 'http://192.168.1.85:15303/sac/asset-management/v3', // url = base url + request url
  baseURL: process.env.VUE_APP_TOOL_API,
  // baseURL:'api',
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers['operation_id'] = store.state.user.userInfo.userId;
    config.headers[HEADER_TOKEN_KEY] = getToken()
      ? getToken()
      : '';
    config.headers[SAC_OTHER_SYSTEM] = true;
    config.headers[SAC_UN] = getSacUn();
    // config.headers[HEADER_TOKEN_KEY] = '4b17760dbd6d43d1bc0912058b7ce1af'
    return config;
  },
  error => {
    // do something with request error
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom status
   * Here is just an example
   * You can also judge the status by HTTP Status status
   */
  response => {
    const res = response.data;
    if (res.cause == 'token过期' || res.cause == '非法token') {
      Message({
        message: res.cause || 'Error',
        type: 'error',
        duration: 5 * 1000
      });
      // window.location.reload();
    }
    // if the custom status is not 20000, it is judged as an error.
    if (response.status !== 200) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      });

      // // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      // if (res.status === 50008 || res.status === 50012 || res.status === 50014) {
      //   // to re-login
      //   MessageBox.confirm(
      //     'You have been logged out, you can cancel to stay on this page, or log in again',
      //     'Confirm logout',
      //     {
      //       confirmButtonText: 'Re-Login',
      //       cancelButtonText: 'Cancel',
      //       type: 'warning'
      //     }
      //   ).then(() => {});
      // }
      return Promise.reject(new Error(res.message || 'Error'));
    } else {
      return Promise.resolve(res);
    }
  },
  error => {
    console.log('err' + error); // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    });
    return Promise.reject(error);
  }
);

export default service;
