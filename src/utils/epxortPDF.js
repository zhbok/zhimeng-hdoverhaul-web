import html2Canvas from 'html2canvas';
import JsPDF from 'jspdf';
/**
 * @param {dom元素} ele
 * @param {pdf名称} pdfName
 */
export const exportPDF = (ele, pdfName) => {
  // 导出之前先将滚动条置顶,不然会出现数据不全的现象
  window.pageYOffset = 0;
  document.documentElement.scrollTop = 0;
  document.body.scrollTop = 0;
  return new Promise((resolve, reject) => {
    ele.style.height = ele.scrollHeight + 'px'; //把要导出的元素高度设置为滚动高度
    ele.style.display = 'table';
    html2Canvas(ele, {
      useCORS: true, //是否尝试使用CORS从服务器加载图像
      allowTaint: true,
      dpi: 300, //解决生产图片模糊
      scale: 3 //清晰度--放大倍数
    })
      .then(canvas => {
        let contentWidth = canvas.width;
        let contentHeight = canvas.height;
        let pageHeight = (contentWidth / 592.28) * 841.89; // 一页pdf显示html页面生成的canvas高度;
        let leftHeight = contentHeight; //未生成pdf的html页面高度
        let position = 0; //pdf页面偏移
        //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
        // let imgWidth = 595.28
        let imgWidth = 560.28; //宽度
        let imgHeight = (592.28 / contentWidth) * contentHeight;
        let pageData = canvas.toDataURL('image/jpeg', 1.0);
        let PDF = new JsPDF('', 'pt', 'a4');
        if (leftHeight < pageHeight) {
          PDF.addImage(pageData, 'JPEG', 20, 20, imgWidth, imgHeight);
        } else {
          while (leftHeight > 0) {
            PDF.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight);
            leftHeight -= pageHeight;
            position -= 841.89;
            if (leftHeight > 0) {
              PDF.addPage();
            }
          }
        }
        PDF.save(pdfName + '.pdf'); //下载标题
        //下载完成后改变浏览器界面可视区域高度
        ele.style.height = ele.clientHeight + 'px';
        resolve('pdf导出成功');
      })
      .catch(error => {
        reject(error);
      });
  });
};
