export function getNowYear() {
  let timestamp = Date.parse(new Date());
  let date = new Date(timestamp);
  //获取年份
  let thieYear = date.getFullYear();
  return thieYear;
}

//获取当前日期
export function getNowDate(type = 1) {
  const now = new Date();
  const year = now.getFullYear();
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const day = String(now.getDate()).padStart(2, '0');
  const hours = String(now.getHours()).padStart(2, '0');
  const minutes = String(now.getMinutes()).padStart(2, '0');
  const seconds = String(now.getSeconds()).padStart(2, '0');
  let formattedNum = '';
  if (type == 1) {
    const formattedTime =
      year +
      '-' +
      month +
      '-' +
      day +
      '' +
      hours +
      ':' +
      minutes +
      ':' +
      seconds;
    formattedNum = formattedTime.replace(/(.{10})/g, '$1 ');
  } else if (type == 2) {
    formattedNum = year + month + day + hours + minutes + seconds;
  } else {
    formattedNum = year + '-' + month + '-' + day;
  }
  return formattedNum;
}
