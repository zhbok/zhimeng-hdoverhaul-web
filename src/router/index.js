import Vue from 'vue';
import VueRouter from 'vue-router';
import Layout from '@/layout';
const originalPush = VueRouter.prototype.push;

VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

Vue.use(VueRouter);
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/overhaul'
  },
  {
    path: '/overhaul',
    name: 'Layout',
    component: Layout,
    redirect: '/overhaul/intelligentMaintenance',
    children: [
      // 智能检修
      {
        path: 'intelligentMaintenance',
        name: 'intelligentMaintenance',
        component: () => import('@/views/newIntelligentMaintenance')
      },
      // 任务池
      {
        path: 'intelligentMaintenance1',
        name: 'intelligentMaintenance1',
        component: () => import('@/views/intelligentMaintenance1')
      },
      // 任务池
      {
        path: 'intelligentMaintenance2',
        name: 'intelligentMaintenance2',
        component: () => import('@/views/intelligentMaintenance2')
      },
      // 任务排版
      {
        path: 'intelligentTypesetting',
        name: 'intelligentTypesetting',
        component: () => import('@/views/intelligentTypesetting')
      },
      /**
       * 非计划性检修
       */
      // 缺陷管理-缺陷任务
      {
        path: 'defectTasks',
        name: 'defectTasks',
        component: () =>
          import('@/views/unplannedMaintenance/defectManage/tasks')
      },
      // 缺陷管理-缺陷工单
      {
        path: 'defectWorkOrder',
        name: 'defectWorkOrder',
        component: () =>
          import('@/views/unplannedMaintenance/defectManage/workOrder')
      },
      // 缺陷管理-缺陷统计分析
      {
        path: 'defectStatistics',
        name: 'defectStatistics',
        component: () =>
          import('@/views/unplannedMaintenance/defectManage/statistics')
      },
      // 特殊检修管理-特殊检修方案
      {
        path: 'specialProgramme',
        name: 'specialProgramme',
        component: () =>
          import(
            '@/views/unplannedMaintenance/specialMaintenanceManage/programme'
          )
      },
      // 特殊检修管理-特殊检修计划
      {
        path: 'specialPlan',
        name: 'specialPlan',
        component: () =>
          import('@/views/unplannedMaintenance/specialMaintenanceManage/plan')
      },
      // 特殊检修管理-特殊检修工单
      {
        path: 'specialWorkOrder',
        name: 'specialWorkOrder',
        component: () =>
          import(
            '@/views/unplannedMaintenance/specialMaintenanceManage/workOrder'
          )
      },
      // 特殊检修管理-特殊结果评估
      {
        path: 'specialResultEvaluation',
        name: 'specialResultEvaluation',
        component: () =>
          import(
            '@/views/unplannedMaintenance/specialMaintenanceManage/resultEvaluation'
          )
      },
      /**
       * 计划性检修
       */
      //  计划性检修项目计划
      {
        path: 'plannedProjectPlan',
        name: 'plannedProjectPlan',
        component: () => import('@/views/plannedMaintenance/projectPlan')
      },
      //  计划性检修项目任务
      {
        path: 'plannedProjectTasks',
        name: 'plannedProjectTasks',
        component: () => import('@/views/plannedMaintenance/projectTasks')
      },
      //  计划性检修项目工单
      {
        path: 'plannedProjectWorkOrder',
        name: 'plannedProjectWorkOrder',
        component: () => import('@/views/plannedMaintenance/projectWorkOrder')
      },
      //  计划性检修结果评估
      {
        path: 'plannedResultEvaluation',
        name: 'plannedResultEvaluation',
        component: () => import('@/views/plannedMaintenance/resultEvaluation')
      },

      /**
       * 设备异动管理
       */
      //  设备异动列表
      {
        path: 'equipmentChangeList',
        name: 'equipmentChangeList',
        component: () => import('@/views/equipmentChangeManage/list')
      },
      /**
       * 检修知识库
       */
      //  缺陷数据字典
      {
        path: 'defectDataDictionary',
        name: 'defectDataDictionary',
        component: () =>
          import('@/views/maintenanceKnowledgeBase/defectDataDictionary')
      },
      //  检修方案库
      {
        path: 'maintenancePlanBase',
        name: 'maintenancePlanBase',
        component: () =>
          import('@/views/maintenanceKnowledgeBase/maintenancePlanBase')
      },
      //  作业指导书
      {
        path: 'workingInstruction',
        name: 'workingInstruction',
        component: () =>
          import('@/views/maintenanceKnowledgeBase/workingInstruction')
      },
      //  检修工序卡
      {
        path: 'maintenanceProcessCard',
        name: 'maintenanceProcessCard',
        component: () =>
          import('@/views/maintenanceKnowledgeBase/maintenanceProcessCard')
      }
    ]
  }
];

export default routes;
