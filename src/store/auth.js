// 权限
import ApiServer from '@/api/operationTicket/operationTicke';

export default {
  namespaced: true,
  state: {
    isCategory: true
  },
  mutations: {
    SET_IS_CATEGORY(state, flag) {
      state.isCategory = flag;
    }
  },
  actions: {
    // 用户有无新增权限
    async postIsUsersByCategory({ commit, rootState }) {
      const { data = [] } = await ApiServer.isUsersByCategory({
        category: 'isOperater'
      });
      const isCategory = data.find(
        item => item.userId === rootState.user.userInfo.userId
      );
      commit('SET_IS_CATEGORY', isCategory);
    }
  }
};
