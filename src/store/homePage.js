
export default {
  namespaced: true,
  state: {
    searchForm: {},
  },
  mutations: {
    SET_SEARCH_FORM(state, data) {
      console.log("******mutations***SET_SEARCH_FORM**********")
      state.searchForm = data;
    }
  },
  actions: {

  }
};