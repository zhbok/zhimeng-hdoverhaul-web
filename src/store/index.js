import Vue from 'vue';
import Vuex from 'vuex';
import user from './user';
import auth from './auth';
import homePage from './homePage';
import VuexPersistence from "vuex-persist";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ["user"],
  key: "vuex_hdoverhaul",
});
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    mainContextHeight: 0
  },
  mutations: {
    SET_MAIN_CONTEXT_HEIGHT(state, height) {
      state.mainContextHeight = height;
    }
  },
  actions: {},
  modules: {
    user,
    auth,
    homePage
  },
  plugins: [vuexLocal.plugin],
});
