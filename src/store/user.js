// 用户信息
import { getToken } from '@/utils/auth';
import publicApi from '@/api/publicApi';
import lodash from 'lodash';
export default {
  state: {
    userInfo: {},
    userLoginInfo: {},
    allOrgInfo: {},
    dataDictionary: {},
    menus: [],
    areaList: [],
    stationList: []
  },
  mutations: {
    SET_USER_INFO(state, user) {
      state.userInfo = user;
    },
    SET_ALLORG_INFO(state, info) {
      state.allOrgInfo = info;
    },
    SET_LOGIN_INFO(state, user) {
      state.userLoginInfo = user;
    },
    SET_MENUS(state, data) {
      state.menus = data;
    },
    SET_DATA_DICTIONARY(state, data) {
      state.dataDictionary = data;
    },
    SET_AREALIST(state, data) {
      state.areaList = data;
    },
    SET_STATIONLIST(state, data) {
      state.stationList = data;
    }
  },
  actions: {
    async getUserInfoOnce({ commit, dispatch }, systemId) {
      return new Promise(async (resolve, reject) => {
        const res1 = await publicApi.getAllOrgIdByUserId({
          systemId: systemId,
          token: getToken()
        });
        const res2 = await publicApi.getUserInfoAndMenus({
          systemId: systemId,
          token: getToken()
        });

        Promise.all([res1, res2]).then(async result => {
          let allOrgInfo = result[0].data;
          let { userLoginInfo, menus } = result[1].data;
          const params = {
            orgId: userLoginInfo.orgInfo.id,
            userId: userLoginInfo.userInfo.id
          };
          publicApi.addUserOrg(params);
          // 当前登录用户已绑定的角色
          localStorage.setItem('roleList', JSON.stringify(userLoginInfo.role));
          // 当前登录用户组织信息
          localStorage.setItem(
            'orgInfo',
            JSON.stringify(userLoginInfo.orgInfo)
          );
          // 当前登录用户个人信息
          let userInfoObj = lodash.cloneDeep(userLoginInfo.userInfo);
          userInfoObj.nickName = userInfoObj.fullname;

          // 获取当前账号所在片区
          let areaObj = {
            pid: allOrgInfo.companyAlias ? allOrgInfo.companyAlias : '',
            region_code:
              !allOrgInfo.companyAlias && allOrgInfo.regionAlias
                ? allOrgInfo.regionAlias
                : ''
          };
          const areaRes = await publicApi.getAreaId(areaObj);
          let areaList = areaRes?.data ?? [];
          areaList.unshift({ areaName: '全部', areaCode: '' });
          // userInfoObj.userId = userInfoObj.account;
          commit('SET_ALLORG_INFO', { ...allOrgInfo });
          commit('SET_MENUS', menus || []);
          commit('SET_USER_INFO', { ...userInfoObj });
          commit('SET_LOGIN_INFO', { ...userLoginInfo });
          commit('SET_AREALIST', [...areaList]);
          // 获取当前账号所在场站
          if (allOrgInfo.areaAlias) {
            const stationRes = await publicApi.getStationsId({
              pid: allOrgInfo.areaAlias
            });
            let stationList = stationRes?.data ?? [];
            commit('SET_STATIONLIST', [...stationList]);
          }
          resolve();
        });
        dispatch('getDataDictionary');
      });
    },
    async getDataDictionary({ commit }) {
      await publicApi.getDictionaryList({ type: '' }).then(res => {
        if (res.code == 200) {
          let dataDictionary = res.data;
          commit('SET_DATA_DICTIONARY', [...dataDictionary]);
        }
      });
    }
  }
};
