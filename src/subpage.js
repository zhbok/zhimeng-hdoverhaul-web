import './public-path';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import routes from './router';
import store from './store';
import QiankunCt from './Qiankun';

Vue.config.productionTip = false;

// new Vue({
//   router,
//   store,
//   render: h => h(App)
// }).$mount('#app')

let instance = null;
let router = null;

function render(props = {}) {
  const { container } = props;
  console.log(props, 'props');

  if (container) {
    router = new VueRouter({
      routes: [
        {
          path: props.prefixRoute,
          component: App,
          children: routes
        }
      ]
    });
  } else {
    router = new VueRouter({
      routes
    });
  }

  instance = new Vue({
    router,
    store,
    render: h => h(container ? QiankunCt : App)
  }).$mount(container ? container.querySelector('#app') : '#app');
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {
  console.log('[vue] vue app bootstraped');
}
export async function mount(props) {
  console.log('[vue] props from main framework', props);
  props.onGlobalStateChange((state, prev) => {
    console.log(state, prev, 'props123');
  });
  render(props);
}
export async function unmount() {
  console.log('[vue] vue app unmount');
}
